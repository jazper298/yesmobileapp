package com.example.yesmobileapp.Utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;

public class Toasters {


    private static ProgressDialog progress;

    public static void ShowToast(Context v, String message)
    {
        Toasty.info(v, "" + message, Toast.LENGTH_LONG).show();
    }

    public static void ShowLoadingSpinner(Context context)
    {
        progress = new ProgressDialog(context);
        progress.setMessage("Loading...");
        progress.setCancelable(false); // disable dismiss by tapping outside of the dialog
        progress.show();
    }

    public static void HideLoadingSpinner()
    {
        progress.dismiss();
    }

    public static void ShowSnackBar(View view, String message)
    {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }
}
