package com.example.yesmobileapp.Utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

public class PopUpProvider {

    public static void buildConfirmationDialog(Context context, DialogInterface.OnClickListener dialogInterface, String title, String message, String yesMessage, String noMessage)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message).setPositiveButton(yesMessage, dialogInterface)
                .setNegativeButton(noMessage, dialogInterface).setTitle(title).show();
    }
}
