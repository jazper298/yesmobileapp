package com.example.yesmobileapp.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.yesmobileapp.Adapters.RankingPagerAdapter;
import com.example.yesmobileapp.Helpers.BottomNavigationViewHelper;
import com.example.yesmobileapp.MainActivity;
import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.PopUpProvider;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import es.dmoral.toasty.Toasty;

public class TeachResultActivity extends AppCompatActivity {
    private static final String TAG = "TeachResultActivity";
    private static final int ACTIVITY_NUM = 2;

    //private Context mContext = RankingsActivity.this;
    private Context context;
    private long backPressedTime;
    private RankingPagerAdapter mRankingPagerAdapter;
    private ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teach_result);
        Log.d(TAG, "onCreate: starting.");
        context = this;

        setupBottomNavigationView();
    }
    /**
     * BottomNavigationView setup
     */
    private void setupBottomNavigationView(){
        Log.d(TAG, "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        //BottomNavigationViewHelper.enableNavigation(mContext, bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);
        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationViewEx.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.ic_house:
                        Intent intent0 = new Intent(TeachResultActivity.this, TeachMainActivity.class);
                        startActivity(intent0);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
//                    case R.id.ic_notes:
//                        Intent intent1 = new Intent(RankingsActivity.this, NotesActivity.class);
//                        startActivity(intent1);
//                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                        finish();
//                        break;
                    case R.id.ic_classmates:
                        Intent intent2 = new Intent(TeachResultActivity.this, TeachClassActivity.class);
                        startActivity(intent2);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                    case R.id.ic_rank:
//                        Intent intent3 = new Intent(RankingsActivity.this, RankingsActivity.class);
//                        startActivity(intent3);
//                        finish();
                        break;
                    case R.id.ic_me:
                        Intent intent4 = new Intent(TeachResultActivity.this, TeachMeActivity.class);
                        startActivity(intent4);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                }
                return false;
            }
        });
    }
    @Override
    public void onBackPressed() {
        if (backPressedTime + 1500 > System.currentTimeMillis()){
            closeSession();
            return;
        }
        else{
            Toasty.info(context, "Press BACK again to exit").show();
        }
        backPressedTime = System.currentTimeMillis();

    }
    private void closeSession() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        TeachResultActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(context, dialogClickListener, "Confirm close ", "Are you sure you want to close application?","Yes", "No");
    }
}
