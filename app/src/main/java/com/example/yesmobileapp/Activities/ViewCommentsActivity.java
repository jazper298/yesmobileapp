package com.example.yesmobileapp.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.example.yesmobileapp.Adapters.ViewCommentsAdapter;
import com.example.yesmobileapp.Models.Comments;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class ViewCommentsActivity extends AppCompatActivity {
    private View view;
    private RecyclerView recyclerView;
    private ListView listView;
    private ArrayList<Comments> commentsList;
    private ViewCommentsAdapter viewCommentsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_comments);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // and this
                startActivity(new Intent(ViewCommentsActivity.this, TeachMainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });
        initializeUI();
    }
    private void initializeUI() {
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        //listView = (ListView)findViewById(R.id.listView);
        commentsList = new ArrayList<>();
        commentsList.add(new Comments(R.drawable.vito1, "Vito Bratta ", "Comments namba 1"));
        commentsList.add(new Comments(R.drawable.james1, "James Hitfield ","Comments namba 2"));
        commentsList.add(new Comments(R.drawable.billy2, "Billy Sheehan ","Comments namba 3"));
        commentsList.add(new Comments(R.drawable.joe1, "Joe Satriani ","Comments namba 4"));
        commentsList.add(new Comments(R.drawable.kirk1, "Kirk Hammett ","Comments namba 5"));


        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        viewCommentsAdapter = new ViewCommentsAdapter(getApplicationContext(), commentsList);

        recyclerView.setAdapter(viewCommentsAdapter);
    }
}
