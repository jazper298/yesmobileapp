package com.example.yesmobileapp.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.yesmobileapp.Adapters.SectionsPagerAdapter;
import com.example.yesmobileapp.Fragments.LessonFragment;
import com.example.yesmobileapp.Fragments.SchoolFragment;
import com.example.yesmobileapp.Fragments.TeachLessonFragment;
import com.example.yesmobileapp.Fragments.VideosFragment;
import com.example.yesmobileapp.Helpers.BottomNavigationViewHelper;
import com.example.yesmobileapp.MainActivity;
import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.PopUpProvider;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import es.dmoral.toasty.Toasty;

public class TeachMainActivity extends AppCompatActivity {
    private static final String TAG = "TeachMainActivity";
    private static final int ACTIVITY_NUM = 0;

    //private Context mContext = MainActivity.this;
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    private Context context;
    private long backPressedTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teach_main);
        Log.d(TAG, "onCreate: starting.");
        context = this;

        setupBottomNavigationView();
        initializeUI();
    }
    /**
     * BottomNavigationView setup
     */
    private void setupBottomNavigationView(){
        Log.d(TAG, "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        //BottomNavigationViewHelper.enableNavigation(mContext, bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(0);
        menuItem.setChecked(true);
        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationViewEx.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.ic_house:
//                        Intent intent0 = new Intent(MainActivity.this, MainActivity.class);
//                        startActivity(intent0);
//                        finish();
                        break;
//                    case R.id.ic_notes:
//                        Intent intent1 = new Intent(MainActivity.this, NotesActivity.class);
//                        startActivity(intent1);
//                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                        finish();
//                        break;
                    case R.id.ic_classmates:
                        Intent intent2 = new Intent(TeachMainActivity.this, TeachClassActivity.class);
                        startActivity(intent2);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                    case R.id.ic_rank:
                        Intent intent3 = new Intent(TeachMainActivity.this, TeachResultActivity.class);
                        startActivity(intent3);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                    case R.id.ic_me:
                        Intent intent4 = new Intent(TeachMainActivity.this, TeachMeActivity.class);
                        startActivity(intent4);
                        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                }
                return false;
            }
        });
    }
    private void initializeUI(){
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager)findViewById(R.id.container1);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_library_books_black_24dp);//.setText("Inquiry");//lesson
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_video_library_black_24dp);//.setText("Teacher");//videos
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_question_answer_black_24dp);//.setText("School");;//quizzes
//        tabLayout.getTabAt(3).setIcon(R.drawable.ic_visibility);//.setText("Policy");;//policy
    }
    private void setupViewPager(ViewPager viewPager){
        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        sectionsPagerAdapter.addFragment(new TeachLessonFragment());
        sectionsPagerAdapter.addFragment(new VideosFragment());
        sectionsPagerAdapter.addFragment(new SchoolFragment());
//        sectionsPagerAdapter.addFragment(new PolicyFragment());
        viewPager.setAdapter(sectionsPagerAdapter);
    }
    @Override
    public void onBackPressed() {
        if (backPressedTime + 1500 > System.currentTimeMillis()){
            closeSession();
            return;
        }
        else{
            Toasty.info(context, "Press BACK again to exit").show();
        }
        backPressedTime = System.currentTimeMillis();

    }
    private void closeSession() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        TeachMainActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(context, dialogClickListener, "Confirm close ", "Are you sure you want to close application?","Yes", "No");
    }
}
