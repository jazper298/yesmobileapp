package com.example.yesmobileapp.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.yesmobileapp.Adapters.RankingPagerAdapter;
import com.example.yesmobileapp.Adapters.SectionsPagerAdapter;
import com.example.yesmobileapp.Fragments.AllTimeFragment;
import com.example.yesmobileapp.Fragments.InquiryFragment;
import com.example.yesmobileapp.Fragments.MonthFragment;
import com.example.yesmobileapp.Fragments.PolicyFragment;
import com.example.yesmobileapp.Fragments.SchoolFragment;
import com.example.yesmobileapp.Fragments.TeacherFragment;
import com.example.yesmobileapp.Fragments.TodayFragment;
import com.example.yesmobileapp.Helpers.BottomNavigationViewHelper;
import com.example.yesmobileapp.MainActivity;
import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.PopUpProvider;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import es.dmoral.toasty.Toasty;

public class RankingsActivity extends AppCompatActivity {
    private static final String TAG = "FindActivity";
    private static final int ACTIVITY_NUM = 2;

    //private Context mContext = RankingsActivity.this;
    private Context context;
    private long backPressedTime;
    private RankingPagerAdapter mRankingPagerAdapter;
    private ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rankings);
        Log.d(TAG, "onCreate: starting.");
        context = this;

        setupBottomNavigationView();
        initializeUI();
    }
    /**
     * BottomNavigationView setup
     */
    private void setupBottomNavigationView(){
        Log.d(TAG, "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        //BottomNavigationViewHelper.enableNavigation(mContext, bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(2);
        menuItem.setChecked(true);
        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationViewEx.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.ic_house:
                        Intent intent0 = new Intent(RankingsActivity.this, MainActivity.class);
                        startActivity(intent0);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
//                    case R.id.ic_notes:
//                        Intent intent1 = new Intent(RankingsActivity.this, NotesActivity.class);
//                        startActivity(intent1);
//                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                        finish();
//                        break;
                    case R.id.ic_classmates:
                        Intent intent2 = new Intent(RankingsActivity.this, ClassmatesActivity.class);
                        startActivity(intent2);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                    case R.id.ic_rank:
//                        Intent intent3 = new Intent(RankingsActivity.this, RankingsActivity.class);
//                        startActivity(intent3);
//                        finish();
                        break;
                    case R.id.ic_me:
                        Intent intent4 = new Intent(RankingsActivity.this, MeActivity.class);
                        startActivity(intent4);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                }
                return false;
            }
        });
    }
    private void initializeUI(){
        mRankingPagerAdapter = new RankingPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager)findViewById(R.id.container1);
        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout)findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        tabLayout.getTabAt(0).setText("Today");//.setIcon(R.drawable.ic_queue);//.setText("Inquiry");//today
        tabLayout.getTabAt(1).setText("Month");//.setIcon(R.drawable.ic_person);//.setText("Teacher");//month
        tabLayout.getTabAt(2).setText("All Time");//.setIcon(R.drawable.ic_account);//.setText("School");;//all time
    }
    private void setupViewPager(ViewPager viewPager){
        RankingPagerAdapter rankingPagerAdapter = new RankingPagerAdapter(getSupportFragmentManager());
        rankingPagerAdapter.addFragment(new TodayFragment());
        rankingPagerAdapter.addFragment(new MonthFragment());
        rankingPagerAdapter.addFragment(new AllTimeFragment());
        viewPager.setAdapter(rankingPagerAdapter);
    }
    @Override
    public void onBackPressed() {
        if (backPressedTime + 1500 > System.currentTimeMillis()){
            closeSession();
            return;
        }
        else{
            Toasty.info(context, "Press BACK again to exit").show();
        }
        backPressedTime = System.currentTimeMillis();

    }
    private void closeSession() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        RankingsActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(context, dialogClickListener, "Confirm close ", "Are you sure you want to close application?","Yes", "No");
    }
}
