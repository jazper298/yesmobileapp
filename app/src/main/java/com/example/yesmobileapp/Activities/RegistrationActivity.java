package com.example.yesmobileapp.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.yesmobileapp.MainActivity;
import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.Debugger;
import com.example.yesmobileapp.Utils.HttpProvider;
import com.example.yesmobileapp.Utils.UserSession;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class RegistrationActivity extends AppCompatActivity {
    MaterialEditText username, email, password;
    Button btn_register;

    FirebaseAuth auth;
    DatabaseReference reference;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
//        getSupportActionBar().hide();
        context = this;

        //Firebase Database
        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        btn_register = findViewById(R.id.btn_register);

        auth = FirebaseAuth.getInstance();

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txt_username = username.getText().toString();
                String txt_email = email.getText().toString();
                String txt_password = password.getText().toString();

                if (TextUtils.isEmpty(txt_username) || TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)){
                    Toast.makeText(RegistrationActivity.this, "All fileds are required", Toast.LENGTH_SHORT).show();
                } else if (txt_password.length() < 6 ){
                    Toast.makeText(RegistrationActivity.this, "password must be at least 6 characters", Toast.LENGTH_SHORT).show();
                } else {
                    register(txt_username, txt_email, txt_password);
                }
            }
        });

    }
    //Firebase Database
    private void register(final String username, String email, String password){

        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            assert firebaseUser != null;
                            String userid = firebaseUser.getUid();

                            reference = FirebaseDatabase.getInstance().getReference("Users").child(userid);

                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", userid);
                            hashMap.put("username", username);
                            hashMap.put("imageURL", "default");
                            hashMap.put("status", "offline");
                            hashMap.put("search", username.toLowerCase());

                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        Intent intent = new Intent(RegistrationActivity.this, TeachMainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            });
                        } else {
                            Toast.makeText(RegistrationActivity.this, "You can't register woth this email or password", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
    @Override
    protected void onStart() {
        super.onStart();
//        initializeUI();
    }

//    private void initializeUI()
//    {
//
//        Button btnSubmit;
//
//        etEmail = (EditText) findViewById(R.id.etEmail);
//        etPassword = (EditText) findViewById(R.id.etPass);
//        etConfirmPassword = (EditText) findViewById(R.id.etConfirmPass);
//
////        etEmail.setText("jazper298@gmail.com");
////        etPassword.setText("metallica298");
//        btnSubmit = (Button) findViewById(R.id.btnSubmit);
//
//        btnSubmit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                register();
//            }
//        });
//    }

//    private void register()
//    {
//        try
//        {
//            //Toasty.success(context, "FUCK").show();
//            JSONObject jsonObject = new JSONObject();
//            final ProgressDialog progressDialog = new ProgressDialog(context);
//            jsonObject.put("email", etEmail.getText().toString());
//            jsonObject.put("password", etPassword.getText().toString());
//            jsonObject.put("confirm_password", etConfirmPassword.getText().toString());
//
//            StringEntity stringEntity = new StringEntity(jsonObject.toString());
//
//            HttpProvider.post(context, "create", stringEntity, "application/json", new JsonHttpResponseHandler(){
//
//                @Override
//                public void onStart() {
//                    super.onStart();
//                    progressDialog.show();
//                }
//
//                @Override
//                public void onFinish() {
//                    super.onFinish();
//                    progressDialog.dismiss();
//                }
//
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                    super.onSuccess(statusCode, headers, response);
//                    Debugger.logD(response.toString());
//                    Toasty.success(context, "Success").show();
//                    UserSession session = new Gson().fromJson(response.toString(), new TypeToken<UserSession>(){}.getType());
//                    session.saveUserSession(getApplicationContext());
//                    Intent main_intent = new Intent(getApplicationContext(), MainActivity.class);
//                    main_intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(main_intent);
//                    finish();
//
//
//                }
//
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
//                    super.onSuccess(statusCode, headers, response);
//                    Debugger.logD(response.toString());
//                    Toasty.success(context, "Success").show();
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                    super.onFailure(statusCode, headers, throwable, errorResponse);
//                    //Debugger.logD("wee" + errorResponse.toString());
//                    Toasty.error(context, "Error From Server 1").show();
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                    super.onFailure(statusCode, headers, throwable, errorResponse);
//                    Toasty.error(context, "Error from Server 2").show();
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                    super.onFailure(statusCode, headers, responseString, throwable);
//                    //Toasty.error(context, "Error from Server 3").show();
//                    Toasty.error(context, responseString).show();
//                }
//
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, String responseString) {
//                    super.onSuccess(statusCode, headers, responseString);
//                }
//            });
//
//        } catch (Exception err)
//        {
//            Toasty.error(context, err.toString()).show();
//        }
//    }
}
