package com.example.yesmobileapp.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.yesmobileapp.Adapters.ClassmatesAdapter;
import com.example.yesmobileapp.Helpers.BottomNavigationViewHelper;
import com.example.yesmobileapp.MainActivity;
import com.example.yesmobileapp.Models.Classmates;
import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.PopUpProvider;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class ClassmatesActivity extends AppCompatActivity {

    private static final String TAG = "ClassmatesActivity";
    private static final int ACTIVITY_NUM = 1;

    //private Context mContext = ClassmatesActivity.this;

    private Context context;
    private long backPressedTime;
    private View view;
    private RecyclerView recyclerView;
    private ArrayList<Classmates> classmatesList;
    private ClassmatesAdapter classmatesAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classmates);
        Log.d(TAG, "onCreate: starting.");
        context = this;

        setupBottomNavigationView();
        initializeUI();
    }
    /**
     * BottomNavigationView setup
     */
    private void setupBottomNavigationView(){
        Log.d(TAG, "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        //BottomNavigationViewHelper.enableNavigation(mContext, bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);

        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationViewEx.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.ic_house:
                        Intent intent0 = new Intent(ClassmatesActivity.this, MainActivity.class);
                        startActivity(intent0);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
//                    case R.id.ic_notes:
//                        Intent intent1 = new Intent(ClassmatesActivity.this, NotesActivity.class);
//                        startActivity(intent1);
//                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                        finish();
//                        break;
                    case R.id.ic_classmates:
//                        Intent intent2 = new Intent(ClassmatesActivity.this, ClassmatesActivity.class);
//                        startActivity(intent2);
//                        finish();
                        break;
                    case R.id.ic_rank:
                        Intent intent3 = new Intent(ClassmatesActivity.this, RankingsActivity.class);
                        startActivity(intent3);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                    case R.id.ic_me:
                        Intent intent4 = new Intent(ClassmatesActivity.this, MeActivity.class);
                        startActivity(intent4);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                }
                return false;
            }
        });

    }
    private void initializeUI() {

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        classmatesList = new ArrayList<>();

        //Static Values
        classmatesList.add(new Classmates(R.drawable.soo_in, "Soo-in-Lee", "Kim-Tae-Hee", "Love Story of Harvard", "learn more"));
        classmatesList.add(new Classmates(R.drawable.john1, "John Petrucci", "Lead Guitar", "Dream Theater", "learn more"));
        classmatesList.add(new Classmates(R.drawable.billy1, "Billy Sheehan", "Bass Guitar", "Mr. Big", "learn more"));
        classmatesList.add(new Classmates(R.drawable.james1, "James Hitfield", "Lead Vocal/Rythm Guitar ", "Metallica", "learn more"));
        classmatesList.add(new Classmates(R.drawable.joe1, "Joe Satriani", "Lead Guitar", "Grand Tour 3", "learn more"));
        classmatesList.add(new Classmates(R.drawable.kirk1, "Kirk Hammett", "Lead Guitar", "Metallica", "learn more"));
        classmatesList.add(new Classmates(R.drawable.marty1, "Marty Friedman", "Lead Guitar", "Megadeth", "learn more"));
        classmatesList.add(new Classmates(R.drawable.save1, "Dave Mustaine", "Lead Vocal/Rythm Guitar", "Megadeth", "learn more"));
        classmatesList.add(new Classmates(R.drawable.paul1, "Paul Gilbert", "Lead Guitar", "Mr. Big", "learn more"));
        classmatesList.add(new Classmates(R.drawable.steve1, "Steve Vai", "Lead Guitar ", "Grand Tour 3", "learn more"));
        classmatesList.add(new Classmates(R.drawable.vito1, "Vito Bratta", "Lead Guitar", "While Lion", "learn more"));
        classmatesList.add(new Classmates(R.drawable.yngwie1, "Yngwie Malmsten", "Lead Guitar", "Grand Tour 3", "learn more"));



        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        classmatesAdapter = new ClassmatesAdapter(getApplicationContext(), classmatesList);

        recyclerView.setAdapter(classmatesAdapter);

    }
    @Override
    public void onBackPressed() {
        if (backPressedTime + 1500 > System.currentTimeMillis()){
            closeSession();
            return;
        }
        else{
            Toasty.info(context, "Press BACK again to exit").show();
        }
        backPressedTime = System.currentTimeMillis();

    }
    private void closeSession() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        ClassmatesActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(context, dialogClickListener, "Confirm close ", "Are you sure you want to close application?","Yes", "No");
    }
}
