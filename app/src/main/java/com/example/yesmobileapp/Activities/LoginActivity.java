package com.example.yesmobileapp.Activities;


import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.yesmobileapp.Adapters.LoginPageAdapter;
import com.example.yesmobileapp.Fragments.LoginStudentFragment;
import com.example.yesmobileapp.Fragments.LoginTeacherFragment;
import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.KeyboardHandler;

public class LoginActivity extends AppCompatActivity {

    private Context context;
    private ViewPager viewPager;
    private LoginPageAdapter loginPageAdapter;
    private TabLayout tabLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = getApplicationContext();
        initializeUI();
        KeyboardHandler.hideKeyboard(LoginActivity.this);
    }
    private void initializeUI(){
        tabLayout = findViewById(R.id.tabLayoutLogin);
        viewPager = findViewById(R.id.vpLogin);
        loginPageAdapter = new LoginPageAdapter(getSupportFragmentManager());
        loginPageAdapter.addFragment(LoginStudentFragment.newInstance(), "S t u d e n t");
        loginPageAdapter.addFragment(LoginTeacherFragment.newInstance(), "E d u c a t o r");
        viewPager.setAdapter(loginPageAdapter);
        tabLayout.setupWithViewPager(viewPager);
    }
}
