package com.example.yesmobileapp.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.Debugger;
import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class TeachLessonActivity extends AppCompatActivity {
    private ImageView ivProfilePicture,ivEllipses1,ivPhoto,ivVideo,iv_Image;
    private EditText txtCreateLesson;
    private Button btnShare;


    private static final int REQUEST_CAMERA = 5;
    private static final int SELECT_FILE = 0;

    private static final int CAMERA_REQUEST_CODE = 200;
    private static final int STORAGE_REQUEST_CODE = 400;
    private static final int IMAGE_PICK_GALLERY_CODE = 1000;
    private static final int IMAGE_PICK_CAMERA_CODE = 1001;
    private static final int REQUEST_IMAGE_CAPTURE = 1;



    String cameraPermission[];
    String storagePermission[];

    Uri image_uri;

    private List<Bitmap> mL;
    private List<Uri> mL1;
    private Uri selectImageUrl;
    private Bitmap imageBitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teach_lesson);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create Lesson");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // and this
                startActivity(new Intent(TeachLessonActivity.this, TeachMainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });
        initializeUI();
    }
    private void initializeUI()
    {
        ivProfilePicture = (ImageView) findViewById(R.id.ivProfilePicture);
        ivEllipses1 = (ImageView)findViewById(R.id.ivEllipses1);
        ivPhoto  = (ImageView)findViewById(R.id.ivPhoto);
        ivVideo = (ImageView)findViewById(R.id.ivVideo);
        //iv_Image = (ImageView)findViewById(R.id.iv_Image);
        txtCreateLesson = (EditText)findViewById(R.id.txtCreateLesson);
        btnShare = (Button)findViewById(R.id.btnShare);

        cameraPermission = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        storagePermission = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageImportDialog();
            }
        });
    }
    private void setImagesGallery(){
        Toasty.error(this, "FUCK", Toast.LENGTH_SHORT, true).show();
        LinearLayout gallery = findViewById(R.id.gallery);
        LayoutInflater inflater = LayoutInflater.from(this);
        for (int i = 0; i < mL.size(); i++){
            View view = inflater.inflate(R.layout.galley_item, gallery, false);

            ImageView imageView = view.findViewById(R.id.imageView7);
            //imageView.setImageBitmap(imageBitmap);
            imageView.setImageResource(i);


            gallery.addView(view);
        }

    }
    private void showImageImportDialog(){

        String items[] = {" Camera ", " Gallery ", "Cancel"};
        android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(this);

        dialog.setTitle("Select Image");
        dialog.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0){
                    if (!checkCameraPermission()){

                        requestCameraPermission();

                    }else{
                        pickCamera();
                    }
                }
                if (which == 1){
                    if(!checkStoragePermission()){
                        requestStoragePermission();
                    }else{
                        pickGallery();
                    }

                }
                if (which == 2) {
                    dialog.dismiss();
                }
            }
        });
        dialog.create().show();
    }
    private void pickGallery() {

        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_GALLERY_CODE);

    }

    private void pickCamera() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, storagePermission, STORAGE_REQUEST_CODE);
    }

    private boolean checkStoragePermission() {
        boolean result2 = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return  result2;
    }

    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, cameraPermission, CAMERA_REQUEST_CODE);
    }

    private boolean checkCameraPermission(){
        boolean result = ContextCompat.checkSelfPermission(this,Manifest.permission.CAMERA) == (PackageManager.PERMISSION_GRANTED);
        boolean result2 = ContextCompat.checkSelfPermission(this,Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return result && result2;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case CAMERA_REQUEST_CODE:
                if(grantResults.length > 0){
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if(cameraAccepted && writeStorageAccepted){
                        pickCamera();
                    }else{
                        Toast.makeText(this, "permission denied ", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case STORAGE_REQUEST_CODE:
                if(grantResults.length > 0){
                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if( writeStorageAccepted){
                        pickGallery();
                    }else{
                        Toast.makeText(this, "permission denied ", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_IMAGE_CAPTURE) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            //setImagesGallery();
            ArrayList<Bitmap> mL = new ArrayList<Bitmap>();
            mL.add(imageBitmap);

            LinearLayout gallery = findViewById(R.id.gallery);
            LayoutInflater inflater = LayoutInflater.from(this);
            for (int i = 0; i < mL.size(); i++) {
                View view = inflater.inflate(R.layout.galley_item, gallery, false);

                ImageView imageView = view.findViewById(R.id.imageView7);
                imageView.setImageBitmap(imageBitmap);

                Debugger.logD("SHIT " + imageBitmap);
                gallery.addView(view);
            }
        }
//        else if (resultCode == Activity.RESULT_OK && requestCode == IMAGE_PICK_GALLERY_CODE) {
//
//            Uri selectImageUrl = data.getData();
//
//            ArrayList<Uri> mL1 = new ArrayList<>();
//            mL1.add(selectImageUrl);
//
//            LinearLayout gallery = (LinearLayout) findViewById(R.id.gallery);
//            LayoutInflater inflater = LayoutInflater.from(this);
//            for (int i = 0; i < mL1.size(); i++) {
//                View view = inflater.inflate(R.layout.galley_item, gallery, false);
//
//                final ImageView imageView = view.findViewById(R.id.imageView7);
//                imageView.setImageURI(selectImageUrl);
//                //imageView.setImageResource(i);
//                Debugger.logD("NIGGA " + selectImageUrl);
//                gallery.addView(view);
//            }
//        }
        else if (requestCode == IMAGE_PICK_GALLERY_CODE && resultCode == RESULT_OK && data != null){
            Uri filepath = data.getData();

            try {
                InputStream inputStream = getContentResolver().openInputStream(filepath);
                Bitmap bitmap = BitmapFactory.decodeStream((inputStream));

                ArrayList<Bitmap> mL = new ArrayList<>();
                mL.add(bitmap);

                LinearLayout gallery = (LinearLayout) findViewById(R.id.gallery);
                LayoutInflater inflater = LayoutInflater.from(this);
                for (int i = 0; i < mL.size(); i++) {
                    View view = inflater.inflate(R.layout.gallery_images, gallery, false);

                    ImageView imageView = view.findViewById(R.id.imageView8);
                    imageView.setImageBitmap(bitmap);

                    Debugger.logD("FUCK " + bitmap);
                    gallery.addView(view);
                }


            } catch (FileNotFoundException e){
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);

    }


}
