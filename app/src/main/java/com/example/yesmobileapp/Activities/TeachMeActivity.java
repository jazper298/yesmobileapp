package com.example.yesmobileapp.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.yesmobileapp.Helpers.BottomNavigationViewHelper;
import com.example.yesmobileapp.MainActivity;
import com.example.yesmobileapp.Models.User;
import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.PopUpProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import de.hdodenhof.circleimageview.CircleImageView;
import es.dmoral.toasty.Toasty;

public class TeachMeActivity extends AppCompatActivity {
    private static final String TAG = "TeachMeActivity";
    private static final int ACTIVITY_NUM = 4;

    //private Context mContext = MeActivity.this;
    private Context context;
    private long backPressedTime;
    private ImageView profileMenu;

    CircleImageView profile_photo;
    TextView display_name;

    DatabaseReference reference;
    FirebaseUser fuser;


    StorageReference storageReference;
    private static final int IMAGE_REQUEST = 1;
    private Uri imageUri;
    private StorageTask uploadTask;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teach_me);
        Log.d(TAG, "onCreate: starting.");
        context = this;
        initializeUI();
        setupBottomNavigationView();
    }

    private void initializeUI()
    {
        //Firebase Database
        profile_photo = findViewById(R.id.profile_photo);
        display_name = findViewById(R.id.display_name);

        storageReference = FirebaseStorage.getInstance().getReference("uploads");

        fuser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users").child(fuser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (getApplicationContext() == null) {
                    return;
                }
                User user = dataSnapshot.getValue(User.class);
                display_name.setText(user.getUsername());
                if (user.getImageURL().equals("default")){
                    profile_photo.setImageResource(R.drawable.soo_in);
                } else {
                    Glide.with(getApplicationContext()).load(user.getImageURL()).into(profile_photo);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        profileMenu = (ImageView)findViewById(R.id.profileMenu);
        profileMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popupMenu = new PopupMenu(context, profileMenu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case  R.id.logout:
                                FirebaseAuth.getInstance().signOut();
                                // change this code beacuse your app will crash
                                startActivity(new Intent(TeachMeActivity.this, StartActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                return true;
                        }

                        return false;
                    }
                });
                popupMenu.inflate(R.menu.menu);
                popupMenu.show();
            }
        });

    }
    /**
     * BottomNavigationView setup
     */
    private void setupBottomNavigationView(){
        Log.d(TAG, "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        //BottomNavigationViewHelper.enableNavigation(mContext, bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(3);
        menuItem.setChecked(true);
        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationViewEx.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.ic_house:
                        Intent intent0 = new Intent(TeachMeActivity.this, TeachMainActivity.class);
                        startActivity(intent0);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
//                    case R.id.ic_notes:
//                        Intent intent1 = new Intent(MeActivity.this, NotesActivity.class);
//                        startActivity(intent1);
//                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                        finish();
//                        break;
                    case R.id.ic_classmates:
                        Intent intent2 = new Intent(TeachMeActivity.this, TeachClassActivity.class);
                        startActivity(intent2);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                    case R.id.ic_rank:
                        Intent intent3 = new Intent(TeachMeActivity.this, TeachResultActivity.class);
                        startActivity(intent3);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                    case R.id.ic_me:
//                        Intent intent4 = new Intent(MeActivity.this, MeActivity.class);
//                        startActivity(intent4);
//                        finish();
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (backPressedTime + 1500 > System.currentTimeMillis()){
            closeSession();
            return;
        }
        else{
            Toasty.info(context, "Press BACK again to exit").show();
        }
        backPressedTime = System.currentTimeMillis();

    }
    private void closeSession() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        TeachMeActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(context, dialogClickListener, "Confirm close ", "Are you sure you want to close application?","Yes", "No");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case  R.id.logout:
                FirebaseAuth.getInstance().signOut();
                // change this code beacuse your app will crash
                startActivity(new Intent(TeachMeActivity.this, StartActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                return true;
        }

        return false;
    }
}
