package com.example.yesmobileapp.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.yesmobileapp.Helpers.BottomNavigationViewHelper;
import com.example.yesmobileapp.MainActivity;
import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.PopUpProvider;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

import es.dmoral.toasty.Toasty;

public class MeActivity extends AppCompatActivity {

    private static final String TAG = "MeActivity";
    private static final int ACTIVITY_NUM = 4;

    //private Context mContext = MeActivity.this;
    private Context context;
    private long backPressedTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_me);
        Log.d(TAG, "onCreate: starting.");
        context = this;

        setupBottomNavigationView();
    }
    /**
     * Check a single permission is it has been verified
     * @param permission
     * @return
     */
    public boolean checkPermissions(String permission){
        Log.d(TAG, "checkPermissions: checking permission: " + permission);

        int permissionRequest = ActivityCompat.checkSelfPermission(MeActivity.this, permission);

        if(permissionRequest != PackageManager.PERMISSION_GRANTED){
            Log.d(TAG, "checkPermissions: \n Permission was not granted for: " + permission);
            return false;
        }
        else{
            Log.d(TAG, "checkPermissions: \n Permission was granted for: " + permission);
            return true;
        }
    }

    /**
     * BottomNavigationView setup
     */
    private void setupBottomNavigationView(){
        Log.d(TAG, "setupBottomNavigationView: setting up BottomNavigationView");
        BottomNavigationViewEx bottomNavigationViewEx = (BottomNavigationViewEx) findViewById(R.id.bottomNavViewBar);
        BottomNavigationViewHelper.setupBottomNavigationView(bottomNavigationViewEx);
        //BottomNavigationViewHelper.enableNavigation(mContext, bottomNavigationViewEx);
        Menu menu = bottomNavigationViewEx.getMenu();
        MenuItem menuItem = menu.getItem(3);
        menuItem.setChecked(true);
        bottomNavigationViewEx.setOnNavigationItemSelectedListener(new BottomNavigationViewEx.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.ic_house:
                        Intent intent0 = new Intent(MeActivity.this, MainActivity.class);
                        startActivity(intent0);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
//                    case R.id.ic_notes:
//                        Intent intent1 = new Intent(MeActivity.this, NotesActivity.class);
//                        startActivity(intent1);
//                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                        finish();
//                        break;
                    case R.id.ic_classmates:
                        Intent intent2 = new Intent(MeActivity.this, ClassmatesActivity.class);
                        startActivity(intent2);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                    case R.id.ic_rank:
                        Intent intent3 = new Intent(MeActivity.this, RankingsActivity.class);
                        startActivity(intent3);
                        //overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                        finish();
                        break;
                    case R.id.ic_me:
//                        Intent intent4 = new Intent(MeActivity.this, MeActivity.class);
//                        startActivity(intent4);
//                        finish();
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (backPressedTime + 1500 > System.currentTimeMillis()){
            closeSession();
            return;
        }
        else{
            Toasty.info(context, "Press BACK again to exit").show();
        }
        backPressedTime = System.currentTimeMillis();

    }
    private void closeSession() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        MeActivity.super.onBackPressed();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:

                        break;
                }
            }
        };

        PopUpProvider.buildConfirmationDialog(context, dialogClickListener, "Confirm close ", "Are you sure you want to close application?","Yes", "No");
    }
}
