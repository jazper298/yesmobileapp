package com.example.yesmobileapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yesmobileapp.Models.Classmates;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class ClassmatesAdapter extends RecyclerView.Adapter<ClassmatesAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Classmates> mList;
    public ClassmatesAdapter(Context context, ArrayList<Classmates> list){
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public ClassmatesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_classmates,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ClassmatesAdapter.ViewHolder viewHolder, int i) {
        final Classmates classModel = mList.get(i);
        final ImageView image = viewHolder.imageView;
        TextView text1, text2, text3;

        text1 = viewHolder.textView1;
        text2 = viewHolder.textView2;
        text3 = viewHolder.textView3;

        image.setImageResource(classModel.getImage());

        text1.setText(classModel.getText1());
        text2.setText(classModel.getText2());
        text3.setText(classModel.getText3());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView textView1, textView2, textView3;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            textView1 = (TextView) itemView.findViewById(R.id.textView1);
            textView2 = (TextView) itemView.findViewById(R.id.textView2);
            textView3 = (TextView) itemView.findViewById(R.id.textView3);
        }
    }
}
