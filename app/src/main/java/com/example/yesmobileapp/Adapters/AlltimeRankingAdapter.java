package com.example.yesmobileapp.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yesmobileapp.Models.Alltime;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class AlltimeRankingAdapter extends RecyclerView.Adapter<AlltimeRankingAdapter.ViewHolder>  {
    private Context mContext;
    private ArrayList<Alltime> mList;
    public AlltimeRankingAdapter(Context context, ArrayList<Alltime> list){
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public AlltimeRankingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.listrow_rankings_alltime,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AlltimeRankingAdapter.ViewHolder holder, int i) {
        final Alltime alltimeModel = mList.get(i);
        ImageView image = holder.imageView;
        CardView post_card_view = holder.post_card_view;
        TextView text1, text2, text3;

        text1 = holder.tv_name;
        text2 = holder.tv_points;

        image.setImageResource(alltimeModel.getImage());

        text1.setText(alltimeModel.getText1());
        text2.setText(alltimeModel.getText2());

        if(i == 0) {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.GREEN);
        }
        else if (i == 1) {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.RED);
        }
        else if (i == 2)
        {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.CYAN);
        }
        else if (i == 3)
        {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.GRAY);
        }
        else if (i == 4)
        {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.YELLOW);
        }
        else if (i == 5)
        {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.MAGENTA);
        }
        else if (i == 6)
        {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.LTGRAY);
        }
        else if (i == 7)
        {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.GRAY);
        }
        else if (i == 8)
        {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.CYAN);
        }
        else if (i == 9)
        {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.BLUE);
        } else if (i == 10)
        {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.DKGRAY);
        }
        else if (i == 11)
        {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.MAGENTA);
        } else if (i == 12)
        {
            ((CardView) holder.post_card_view).setCardBackgroundColor(Color.GREEN);
        }


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView tv_name, tv_points;
        CardView post_card_view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_points = (TextView) itemView.findViewById(R.id.tv_points);
            post_card_view = (CardView)itemView.findViewById(R.id.post_card_view);
        }
    }
}
