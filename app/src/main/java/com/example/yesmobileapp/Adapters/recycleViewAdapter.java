package com.example.yesmobileapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yesmobileapp.Models.recycleView;
import com.example.yesmobileapp.R;

import java.util.List;

public class recycleViewAdapter extends RecyclerView.Adapter<recycleViewAdapter.ViewHolder>{
    private Context context;
    private List<recycleView> list;

    public recycleViewAdapter(Context context, List<recycleView> list) {
        this.list = list;
        this.context = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.layout_recycle_view, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final recycleView recycleView = list.get(i);
        final ImageView imageView7 = viewHolder.imageView7;

        imageView7.setImageResource(recycleView.getImage1());
    }


    @Override
    public int getItemCount() {

        return list.size();
    }

    public  class ViewHolder extends RecyclerView.ViewHolder{

        public ImageView imageView7;

        public ViewHolder(View itemView) {
            super(itemView);

            imageView7 = itemView.findViewById(R.id.imageView7);
        }
    }
}
