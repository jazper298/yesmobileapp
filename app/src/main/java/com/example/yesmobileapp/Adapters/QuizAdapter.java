package com.example.yesmobileapp.Adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.yesmobileapp.Models.Quiz;
import com.example.yesmobileapp.Models.Videos;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class QuizAdapter extends RecyclerView.Adapter<QuizAdapter.ViewHolder> {
    private View view;
    private Context mContext;
    private ArrayList<Quiz> mList;
    public QuizAdapter(Context context, ArrayList<Quiz> list){
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public QuizAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        view = layoutInflater.inflate(R.layout.listrow_quiz,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull QuizAdapter.ViewHolder viewHolder, int i) {
        final Quiz quizModel = mList.get(i);
        final ImageView ivProfilePicture = viewHolder.ivProfilePicture;
        final ImageView ivEllipses = viewHolder.ivEllipses;

        TextView txtProfileName, txtQuizTitle, txtClock, txtItems;

        txtProfileName = viewHolder.txtProfileName;
        txtQuizTitle = viewHolder.txtQuizTitle;
        txtClock = viewHolder.txtClock;
        txtItems = viewHolder.txtItems;

        ivProfilePicture.setImageResource(quizModel.getProfilePicture());
        ivEllipses.setImageResource(quizModel.getEllipses());

        txtProfileName.setText(quizModel.getProfileName());
        txtQuizTitle.setText(quizModel.getQuizTitle());
        txtClock.setText(quizModel.getClock());
        txtItems.setText(quizModel.getItems());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView ivProfilePicture, ivEllipses;
        TextView txtProfileName, txtQuizTitle, txtClock,txtItems;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivProfilePicture = (ImageView) itemView.findViewById(R.id.ivProfilePicture);
            ivEllipses = (ImageView) itemView.findViewById(R.id.ivEllipses);

            txtProfileName = (TextView) itemView.findViewById(R.id.txtProfileName);
            txtQuizTitle = (TextView) itemView.findViewById(R.id.txtQuizTitle);
            txtClock = (TextView) itemView.findViewById(R.id.txtClock);
            txtItems = (TextView) itemView.findViewById(R.id.txtItems);

        }
    }
}
