package com.example.yesmobileapp.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.yesmobileapp.Activities.ViewCommentsActivity;
import com.example.yesmobileapp.Fragments.ViewCommentsFragment;
import com.example.yesmobileapp.Models.Lesson;
import com.example.yesmobileapp.Models.recycleView;
import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.Heart;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

import static android.support.constraint.Constraints.TAG;

public class TeachLessonAdapter extends RecyclerView.Adapter<TeachLessonAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Lesson> mList;

    private GestureDetector mGestureDetector;
    private Heart mHeart;

    private static final int NUM_COLUMNS = 2;
    private ArrayList<recycleView> recyclerViewList;
    private recycleViewAdapter recycleViewAdapter;

    public TeachLessonAdapter(Context context, ArrayList<Lesson> list){
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public TeachLessonAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.layout_main_lesson,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull TeachLessonAdapter.ViewHolder viewHolder, int i) {
        final Lesson lessonModel = mList.get(i);
        final ImageView ivProfilePicture = viewHolder.ivProfilePicture;
        final ImageView ivEllipses = viewHolder.ivEllipses;
        //final ImageView ivMainImage = viewHolder.ivMainImage;
        final ImageView ivHeartRed = viewHolder.ivHeartRed;
        final ImageView ivHeartWhite = viewHolder.ivHeartWhite;
        final ImageView ivSpeechBubble = viewHolder.ivSpeechBubble;
        final RecyclerView recyclerView = viewHolder.recyclerView;
        TextView txtProfileName, txtLikes, txtHashtag, txtViewComment, txtTimePosted;

        txtProfileName = viewHolder.txtProfileName;
        txtLikes = viewHolder.txtLikes;
        txtHashtag = viewHolder.txtHashtag;
        txtViewComment = viewHolder.txtViewComment;
        txtTimePosted = viewHolder.txtTimePosted;

        ivProfilePicture.setImageResource(lessonModel.getProfilePicture());
        ivEllipses.setImageResource(lessonModel.getEllipses());
        //ivMainImage.setImageResource(lessonModel.getMainImage());
        ivHeartRed.setImageResource(lessonModel.getHeartRed());
        ivHeartWhite.setImageResource(lessonModel.getHeartWhite());
        ivSpeechBubble.setImageResource(lessonModel.getSpeechBubble());

        txtProfileName.setText(lessonModel.getProfileName());
        txtLikes.setText(lessonModel.getLikes());
        txtHashtag.setText(lessonModel.getHastag());
        txtViewComment.setText(lessonModel.getViewComment());
        txtTimePosted.setText(lessonModel.getTimePosted());



        txtViewComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ViewCommentsActivity.class);
                mContext.startActivity(intent);
//                ViewCommentsFragment fragment2 = new ViewCommentsFragment();
//                FragmentManager fragmentManager = getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.relative, fragment2);
//                fragmentTransaction.commit();
//                final Context context=v.getContext();
//                LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
//                View view = inflater.inflate(R.layout.fragment_view_comments, null);
//
//                final Dialog mBottomSheetDialog = new Dialog (context);
//                Window window = mBottomSheetDialog.getWindow();
//                WindowManager.LayoutParams wlp = window.getAttributes();
//                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
//                window.setAttributes(wlp);
//
////                ImageView iv_imageLesson;
////                iv_imageLesson= (ImageView) view.findViewById(R.id.iv_imageLesson);
////
////                // BottomSheet
////
//                mBottomSheetDialog.setContentView (view);
//                mBottomSheetDialog.setCancelable (true);
//                mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT);
//                mBottomSheetDialog.getWindow ().setGravity (Gravity.TOP);
//                mBottomSheetDialog.show ();
            }
        });
//        ivMainImage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final Context context=v.getContext();
//                LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
//                View view = inflater.inflate(R.layout.dialog_image_lesson, null);
//
//                final Dialog mBottomSheetDialog = new Dialog (context);
//                Window window = mBottomSheetDialog.getWindow();
//                WindowManager.LayoutParams wlp = window.getAttributes();
//                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
//                window.setAttributes(wlp);
//
//                ImageView iv_imageLesson;
//                iv_imageLesson= (ImageView) view.findViewById(R.id.iv_imageLesson);
//
//                // BottomSheet
//
//                mBottomSheetDialog.setContentView (view);
//                mBottomSheetDialog.setCancelable (true);
//                mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
//                        LinearLayout.LayoutParams.WRAP_CONTENT);
//                mBottomSheetDialog.getWindow ().setGravity (Gravity.TOP);
//                mBottomSheetDialog.show ();
//
//                iv_imageLesson.setImageResource(lessonModel.getMainImage());
//            }
//        });
        txtProfileName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openDialogSheet(v);
            }
            private  void openDialogSheet(View v){

                final Context context=v.getContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                View view = inflater.inflate(R.layout.dialog_main_lesson, null);

//                ImageView ivProfilePicture, ivEllipses, ivMainImage,  ivHeartRed, ivHeartWhite, ivSpeechBubble;
//                TextView txtProfileName, txtLikes, txtHashtag, txtViewComment, txtTimePosted;
//                ivProfilePicture = (ImageView) view.findViewById(R.id.ivProfilePicture);
//                ivEllipses = (ImageView) view.findViewById(R.id.ivEllipses);
//                ivMainImage = (ImageView) view.findViewById(R.id.ivMainImage);
//                ivHeartRed = (ImageView) view.findViewById(R.id.ivHeartRed);
//                ivHeartWhite = (ImageView) view.findViewById(R.id.ivHeartWhite);
//                ivSpeechBubble = (ImageView) view.findViewById(R.id.ivSpeechBubble);
//                txtProfileName = (TextView) view.findViewById(R.id.txtProfileName);
//                txtLikes = (TextView) view.findViewById(R.id.txtLikes);
//                txtHashtag = (TextView) view.findViewById(R.id.txtHashtag);
//                txtViewComment = (TextView) view.findViewById(R.id.txtViewComment);
//                txtTimePosted = (TextView) view.findViewById(R.id.txtTimePosted);

                //ImageView imageView = (ImageView)view.findViewById(R.id.imageView);


                final Dialog mBottomSheetDialog = new Dialog (context);
                Window window = mBottomSheetDialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);

                // BottomSheet

                mBottomSheetDialog.setContentView (view);
                mBottomSheetDialog.setCancelable (true);
                mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow ().setGravity (Gravity.TOP);
                mBottomSheetDialog.show ();
//                tvTopic.setText(cpdMonitoring.getTopic()) ;
//                tvDate.setText(cpdMonitoring.getDate()) ;
//                tvVenue.setText(cpdMonitoring.getVenue()) ;
//                tvTime.setText(cpdMonitoring.getTimeFrom() + " - " + cpdMonitoring.getTimeTo()) ;
//                btnDownload.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Toasty.warning(context, "No available certificate yet");
//                    }
//                });


            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
//        private GestureDetector mGestureDetector;
//        private Heart mHeart;
        ImageView ivProfilePicture, ivEllipses, ivMainImage,  ivHeartRed, ivHeartWhite, ivSpeechBubble;
        ImageView ivPhoto, ivVideo;
        TextView txtProfileName, txtLikes, txtHashtag, txtViewComment, txtTimePosted;
        TextView txtCreateLesson;

        RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivProfilePicture = (ImageView) itemView.findViewById(R.id.ivProfilePicture);
            ivEllipses = (ImageView) itemView.findViewById(R.id.ivEllipses);
          //  ivMainImage = (ImageView) itemView.findViewById(R.id.ivMainImage);
            ivHeartRed = (ImageView) itemView.findViewById(R.id.ivHeartRed);
            ivHeartWhite = (ImageView) itemView.findViewById(R.id.ivHeartWhite);
            ivSpeechBubble = (ImageView) itemView.findViewById(R.id.ivSpeechBubble);
            txtProfileName = (TextView) itemView.findViewById(R.id.txtProfileName);
            txtLikes = (TextView) itemView.findViewById(R.id.txtLikes);
            txtHashtag = (TextView) itemView.findViewById(R.id.txtHashtag);
            txtViewComment = (TextView) itemView.findViewById(R.id.txtViewComment);
            txtTimePosted = (TextView) itemView.findViewById(R.id.txtTimePosted);
            recyclerView = (RecyclerView)itemView.findViewById(R.id.recycler_view2);

            ivPhoto = (ImageView) itemView.findViewById(R.id.ivPhoto);
            ivVideo = (ImageView) itemView.findViewById(R.id.ivVideo);
            txtCreateLesson = (TextView) itemView.findViewById(R.id.txtCreateLesson);

            ivHeartWhite.setVisibility(View.VISIBLE);
            ivHeartRed.setVisibility(View.GONE);
            mHeart = new Heart(ivHeartWhite, ivHeartRed);
            mGestureDetector = new GestureDetector(mContext, new GestureListener());
            testToggle();
            viewPost();




        }

        private void testToggle(){
            ivHeartRed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            ivHeartRed.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return mGestureDetector.onTouchEvent(event);
                }
            });
            ivHeartWhite.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return mGestureDetector.onTouchEvent(event);
                }
            });
        }
        public class GestureListener extends GestureDetector.SimpleOnGestureListener{
            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onDoubleTap(MotionEvent e) {
                Log.d(TAG, "onDoubleTap: double tap detected.");
                mHeart.toggleLike();


                return true;
            }
        }
        private void viewPost(){
            recyclerViewList = new ArrayList<>();
            //recyclerViewList.add(new recycleView(R.drawable.goku1,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.soo_in,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.soo_in,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.soo_in,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.soo_in,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.soo_in,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.soo_in,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.soo_in,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.soo_in,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.soo_in,R.drawable.goku1,R.drawable.goku1) );


            StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUMNS,LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(staggeredGridLayoutManager);

            recycleViewAdapter = new recycleViewAdapter( mContext, recyclerViewList);

            recyclerView.setAdapter(recycleViewAdapter);
        }
    }

}
