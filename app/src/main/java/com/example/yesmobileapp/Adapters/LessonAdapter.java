package com.example.yesmobileapp.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.yesmobileapp.Fragments.LessonFragment;
import com.example.yesmobileapp.Models.Classmates;
import com.example.yesmobileapp.Models.Lesson;
import com.example.yesmobileapp.Models.recycleView;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class LessonAdapter extends RecyclerView.Adapter<LessonAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Lesson> mList;
    private static final int NUM_COLUMNS = 3;
    private ArrayList<recycleView> recyclerViewList;
    private recycleViewAdapter recycleViewAdapter;
    public LessonAdapter(Context context, ArrayList<Lesson> list){
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public LessonAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.layout_main_lesson,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull LessonAdapter.ViewHolder viewHolder, int i) {
        final Lesson lessonModel = mList.get(i);
        final ImageView ivProfilePicture = viewHolder.ivProfilePicture;
        final ImageView ivEllipses = viewHolder.ivEllipses;
        //final ImageView ivMainImage = viewHolder.ivMainImage;
        final ImageView ivHeartRed = viewHolder.ivHeartRed;
        final ImageView ivHeartWhite = viewHolder.ivHeartWhite;
        final ImageView ivSpeechBubble = viewHolder.ivSpeechBubble;
        final RecyclerView recyclerView = viewHolder.recyclerView;
        TextView txtProfileName, txtLikes, txtHashtag, txtViewComment, txtTimePosted;

        txtProfileName = viewHolder.txtProfileName;
        txtLikes = viewHolder.txtLikes;
        txtHashtag = viewHolder.txtHashtag;
        txtViewComment = viewHolder.txtViewComment;
        txtTimePosted = viewHolder.txtTimePosted;

        ivProfilePicture.setImageResource(lessonModel.getProfilePicture());
        ivEllipses.setImageResource(lessonModel.getEllipses());
//        ivMainImage.setImageResource(lessonModel.getMainImage());
        ivHeartRed.setImageResource(lessonModel.getHeartRed());
        ivHeartWhite.setImageResource(lessonModel.getHeartWhite());
        ivSpeechBubble.setImageResource(lessonModel.getSpeechBubble());

        txtProfileName.setText(lessonModel.getProfileName());
        txtLikes.setText(lessonModel.getLikes());
        txtHashtag.setText(lessonModel.getHastag());
        txtViewComment.setText(lessonModel.getViewComment());
        txtTimePosted.setText(lessonModel.getTimePosted());

        txtViewComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                LessonFragment fragment2 = new LessonFragment();
//                FragmentManager fragmentManager = getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.frag, fragment2);
//                fragmentTransaction.commit();
                final Context context=v.getContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                View view = inflater.inflate(R.layout.fragment_view_comments, null);

                ImageView backArrow = view.findViewById(R.id.backArrow);

                final Dialog mBottomSheetDialog = new Dialog (context);
                Window window = mBottomSheetDialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);

//                ImageView iv_imageLesson;
//                iv_imageLesson= (ImageView) view.findViewById(R.id.iv_imageLesson);
//
//                // BottomSheet
//
                mBottomSheetDialog.setContentView (view);
                mBottomSheetDialog.setCancelable (true);
                mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow ().setGravity (Gravity.TOP);
                mBottomSheetDialog.show ();

                backArrow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBottomSheetDialog.hide();
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView ivProfilePicture, ivEllipses, ivMainImage,  ivHeartRed, ivHeartWhite, ivSpeechBubble;
        TextView txtProfileName, txtLikes, txtHashtag, txtViewComment, txtTimePosted;
        RecyclerView recyclerView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivProfilePicture = (ImageView) itemView.findViewById(R.id.ivProfilePicture);
            ivEllipses = (ImageView) itemView.findViewById(R.id.ivEllipses);
          //  ivMainImage = (ImageView) itemView.findViewById(R.id.ivMainImage);
            ivHeartRed = (ImageView) itemView.findViewById(R.id.ivHeartRed);
            ivHeartWhite = (ImageView) itemView.findViewById(R.id.ivHeartWhite);
            ivSpeechBubble = (ImageView) itemView.findViewById(R.id.ivSpeechBubble);
            txtProfileName = (TextView) itemView.findViewById(R.id.txtProfileName);
            txtLikes = (TextView) itemView.findViewById(R.id.txtLikes);
            txtHashtag = (TextView) itemView.findViewById(R.id.txtHashtag);
            txtViewComment = (TextView) itemView.findViewById(R.id.txtViewComment);
            txtTimePosted = (TextView) itemView.findViewById(R.id.txtTimePosted);
            recyclerView = (RecyclerView)itemView.findViewById(R.id.recycler_view2);
            recyclerViewList = new ArrayList<>();
            recyclerViewList.add(new recycleView(R.drawable.goku1,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.soo_in,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.kirk1,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.james1,R.drawable.goku1,R.drawable.goku1) );
            recyclerViewList.add(new recycleView(R.drawable.save1,R.drawable.goku1,R.drawable.goku1) );

//            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
//            RecyclerView.LayoutManager rvLayoutManager = layoutManager;
//            recyclerView.setLayoutManager(rvLayoutManager);

            StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(NUM_COLUMNS, LinearLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(staggeredGridLayoutManager);

            recycleViewAdapter = new recycleViewAdapter( mContext, recyclerViewList);

            recyclerView.setAdapter(recycleViewAdapter);

        }
    }
}
