package com.example.yesmobileapp.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.yesmobileapp.Models.Comments;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class ViewCommentsAdapter extends RecyclerView.Adapter<ViewCommentsAdapter.ViewHolder> {
    private Context mContext;
    private ArrayList<Comments> mList;
    public ViewCommentsAdapter(Context context, ArrayList<Comments> list){
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        View view = layoutInflater.inflate(R.layout.layout_comment,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewCommentsAdapter.ViewHolder viewHolder, int i) {
        final Comments commentsModel = mList.get(i);
        ImageView image = viewHolder.comment_profile_image;
        TextView text1, text2;

        text1 = viewHolder.comment_username;
        text2 = viewHolder.comment;

        image.setImageResource(commentsModel.getImage());

        text1.setText(commentsModel.getText1());
        text2.setText(commentsModel.getText2());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView comment_profile_image;
        TextView comment_username, comment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            comment_profile_image = (ImageView) itemView.findViewById(R.id.comment_profile_image);
            comment_username = (TextView) itemView.findViewById(R.id.comment_username);
            comment = (TextView) itemView.findViewById(R.id.comment);
        }
    }
}
