package com.example.yesmobileapp.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.yesmobileapp.Activities.ViewCommentsActivity;
import com.example.yesmobileapp.Models.Lesson;
import com.example.yesmobileapp.Models.Videos;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class VideoAdapter  extends RecyclerView.Adapter<VideoAdapter.ViewHolder> {
    private View view;
    private Context mContext;
    private ArrayList<Videos> mList;
    public VideoAdapter(Context context, ArrayList<Videos> list){
        mContext = context;
        mList = list;
    }
    @NonNull
    @Override
    public VideoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);

        view = layoutInflater.inflate(R.layout.listrow_videos,viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull VideoAdapter.ViewHolder viewHolder, int i) {
        final Videos videosModel = mList.get(i);
        final ImageView ivProfilePicture = viewHolder.ivProfilePicture;
        final ImageView ivEllipses = viewHolder.ivEllipses;
        //final ImageView ivMainVideo = viewHolder.ivMainVideo;
        final VideoView ivMainVideos = viewHolder.ivMainVideos;
        TextView txtProfileName, txtDescription, txtViewComment;

        txtProfileName = viewHolder.txtProfileName;
        txtDescription = viewHolder.txtDescription;
        txtViewComment = viewHolder.txtViewComment;

        ivProfilePicture.setImageResource(videosModel.getProfilePicture());
        ivEllipses.setImageResource(videosModel.getEllipses());
        ivMainVideos.setVideoURI(Uri.parse("android.resource://" + mContext.getPackageName() + "/" + videosModel.getMainVideo()));
//
//        MediaController mediaController = new MediaController(mContext);
//        ivMainVideos.setMediaController(mediaController);
//        mediaController.setAnchorView(ivMainVideos);

        txtProfileName.setText(videosModel.getProfileName());
        txtDescription.setText(videosModel.getDescription());
        txtViewComment.setText(videosModel.getViewComment());
        txtViewComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ViewCommentsActivity.class);
                mContext.startActivity(intent);
            }
        });

        ivMainVideos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogSheet(v);
            }
            private  void openDialogSheet(View v){

                final Context context=v.getContext();
                LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
                View view = inflater.inflate(R.layout.dialog_videos, null);
                final VideoView ivMainVideos = (VideoView) view.findViewById(R.id.ivMainVideos);




                final Dialog mBottomSheetDialog = new Dialog (context);
                Window window = mBottomSheetDialog.getWindow();
                WindowManager.LayoutParams wlp = window.getAttributes();
                wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
                window.setAttributes(wlp);

                // BottomSheet

                mBottomSheetDialog.setContentView (view);
                mBottomSheetDialog.setCancelable (true);
                mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow ().setGravity (Gravity.CENTER);
                mBottomSheetDialog.show ();

//                ivMainVideos.setVideoURI(Uri.parse("android.resource://" + context.getPackageName() + "/" + videosModel.getMainVideo()));

//                ivMainVideos.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        MediaController mediaController = new MediaController(context);
//                        ivMainVideos.setMediaController(mediaController);
//                        mediaController.setAnchorView(ivMainVideos);
//                    }
//                });
                final MediaController mediaController = new MediaController(context);
                ivMainVideos.setMediaController(mediaController);
                mediaController.setAnchorView(ivMainVideos);
                Uri video = Uri.parse("android.resource://" + context.getPackageName() + "/" + videosModel.getMainVideo());
                ivMainVideos.setMediaController(mediaController);
                ivMainVideos.setVideoURI(video);
                ivMainVideos.requestFocus();
                ivMainVideos.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mediaController.show();
                        ivMainVideos.start();
                    }
                });
                ivMainVideos.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra) {
                        return false;
                    }
                });
                ivMainVideos.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder{
        ImageView ivProfilePicture, ivEllipses, ivMainVideo;
        VideoView ivMainVideos;
        TextView txtProfileName, txtDescription, txtViewComment;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ivProfilePicture = (ImageView) itemView.findViewById(R.id.ivProfilePicture);
            ivEllipses = (ImageView) itemView.findViewById(R.id.ivEllipses);
            //ivMainVideo = (ImageView) itemView.findViewById(R.id.ivMainVideo);
            ivMainVideos = (VideoView) itemView.findViewById(R.id.ivMainVideos);

            txtProfileName = (TextView) itemView.findViewById(R.id.txtProfileName);
            txtDescription = (TextView) itemView.findViewById(R.id.txtDescription);
            txtViewComment = (TextView) itemView.findViewById(R.id.txtViewComment);

        }
    }
}
