package com.example.yesmobileapp.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.SparseArray;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.yesmobileapp.Activities.MeActivity;
import com.example.yesmobileapp.Activities.TeachLessonActivity;
import com.example.yesmobileapp.Adapters.LessonAdapter;
import com.example.yesmobileapp.Adapters.TeachLessonAdapter;
import com.example.yesmobileapp.Adapters.recycleViewAdapter;
import com.example.yesmobileapp.MainActivity;
import com.example.yesmobileapp.Models.Lesson;
import com.example.yesmobileapp.Models.User;
import com.example.yesmobileapp.Models.recycleView;
import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.Heart;
import com.example.yesmobileapp.Utils.Permissions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class TeachLessonFragment extends Fragment {
    private static final String TAG = "LessonFragment";
    private View view;
    private Context context;
    private RecyclerView recyclerView;
    private ArrayList<Lesson> lessonsList;
    private TeachLessonAdapter teachlessonAdapter;

    private TextView txtCreateLesson;
    private ImageView iv_Image;
    private FloatingActionButton floatingActionButton;
    private ImageView mBackArrow, mEllipses, mHeartRed, mHeartWhite, mProfileImage, mComment;


    private GestureDetector mGestureDetector;
    private Heart mHeart;

    private static final int REQUEST_CAMERA = 5;
    private static final int SELECT_FILE = 0;
    private StaggeredGridLayoutManager _sGridLayoutManager;
    private static final int CAMERA_REQUEST_CODE = 200;
    private static final int STORAGE_REQUEST_CODE = 400;
    private static final int IMAGE_PICK_GALLERY_CODE = 1000;
    private static final int IMAGE_PICK_CAMERA_CODE = 1001;

    String cameraPermission[];
    String storagePermission[];

    Uri image_uri;
    CircleImageView ivProfilePicture;
    TextView username;

    DatabaseReference reference;
    FirebaseUser fuser;

    StorageReference storageReference;
    private static final int IMAGE_REQUEST = 1;
    private Uri imageUri;
    private StorageTask uploadTask;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedIntanceState){

        view = inflater.inflate(R.layout.fragment_teach_lesson,container,false);
        context = getContext();

        return view;
    }
    @SuppressLint("RestrictedApi")
    @Override
    public void onStart() {
        super.onStart();
//        floatingActionButton =  view.findViewById(R.id.fab);
////        floatingActionButton = ((MainActivity) getActivity()).getFloatingActionButton();
//        floatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
////                if (getActivity() == null) return;
////                Intent intent = new Intent(view.getContext(), TeachLessonActivity.class);
////                startActivity(intent);
//                openDialogSheet(v);
//            }
//        });
        initializeUI();
    }

    private void initializeUI(){
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        txtCreateLesson = (TextView)view.findViewById(R.id.txtCreateLesson);
        ivProfilePicture = view.findViewById(R.id.ivProfilePicture);


        storageReference = FirebaseStorage.getInstance().getReference("uploads");

        fuser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users").child(fuser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (getActivity() == null) {
                    return;
                }
                User user = dataSnapshot.getValue(User.class);
                txtCreateLesson.setText("What's your lesson, " + user.getUsername() + "?");
                if (user.getImageURL().equals("default")){
                    ivProfilePicture.setImageResource(R.drawable.soo_in);
                } else {
                    Glide.with(getContext()).load(user.getImageURL()).into(ivProfilePicture);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        lessonsList = new ArrayList<>();
        lessonsList.add(new Lesson(R.drawable.goku1, R.drawable.ic_ellipses,R.drawable.soo_in,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"Soo-in-Lee ", "Liked by Goku, Vegeta, Metallica and 30 others ", "#kpop #soo-in #loveleeyy", "View all 3 comments", "3 DAYS AGO"));
        lessonsList.add(new Lesson(R.drawable.john1, R.drawable.ic_ellipses,R.drawable.goku1,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"John Petrucci ", "700 Likes", "#Dream Theater", "View All 6 Comments", "3 DAYS AGO"));
        lessonsList.add(new Lesson(R.drawable.soo_in, R.drawable.ic_ellipses,R.drawable.tony,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"Soo-in-Lee ", "1 M You and 1 million Others ", "#Tony #Rockstar #Gwapo", "View All 1M Comments", "1 DAY AGO"));
        lessonsList.add(new Lesson(R.drawable.vito1, R.drawable.ic_ellipses,R.drawable.ard,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"Vito Bratta ", "1 B Likes ", "#Gradwaiting #Sila", "View All 1B Comments", "1 HOUR AGO"));
        lessonsList.add(new Lesson(R.drawable.james1, R.drawable.ic_ellipses,R.drawable.kirk1,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"James Hitfield ", "Liked by You and 200 Others", "#Metallica", "View All 100 Comments", "6 DAYS AGO"));
        lessonsList.add(new Lesson(R.drawable.save1, R.drawable.ic_ellipses,R.drawable.marty1,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"Dave Mustaine ", "Liked by You and 180 Others ", "#Megadeth #Metal", "View All 500 Comments", "2 DAYS AGO"));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        teachlessonAdapter = new TeachLessonAdapter(getContext(), lessonsList);

        recyclerView.setAdapter(teachlessonAdapter);

        txtCreateLesson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openDialogSheet(v);
                Intent intent = new Intent(getActivity(), TeachLessonActivity.class);
                startActivity(intent);
            }
        });


    }

    private  void openDialogSheet(View v){
        Uri imageUri;
        final Context context=v.getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View view = inflater.inflate(R.layout.dialog_main_lesson, null);

        cameraPermission = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

        storagePermission = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};

        final ImageView ivProfilePicture = (ImageView) view.findViewById(R.id.ivProfilePicture);
        final ImageView ivEllipses1 = (ImageView)view.findViewById(R.id.ivEllipses1);
        final ImageView ivPhoto  = (ImageView)view.findViewById(R.id.ivPhoto);
        final ImageView ivVideo = (ImageView)view.findViewById(R.id.ivVideo);
        final ImageView iv_Image = (ImageView)view.findViewById(R.id.iv_Image);
        final EditText txtCreateLesson = (EditText)view.findViewById(R.id.txtCreateLesson);
        Button btnShare = (Button)view.findViewById(R.id.btnShare);

        storageReference = FirebaseStorage.getInstance().getReference("uploads");

        fuser = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users").child(fuser.getUid());

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                txtCreateLesson.setText("What's your lesson, " + user.getUsername() + "?");
                if (user.getImageURL().equals("default")){
                    ivProfilePicture.setImageResource(R.drawable.soo_in);
                } else {
                    //Glide.with(getContext()).load(user.getImageURL()).into(image_profile);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        final Dialog mBottomSheetDialog = new Dialog (context);
        Window window = mBottomSheetDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);

        // BottomSheet

        mBottomSheetDialog.setContentView (view);
        mBottomSheetDialog.setCancelable (true);
        mBottomSheetDialog.getWindow ().setLayout (LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        mBottomSheetDialog.getWindow ().setGravity (Gravity.CENTER);
        mBottomSheetDialog.show ();
        ivEllipses1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBottomSheetDialog.dismiss();
            }
        });
        ivPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //selectImage();
                showImageImportDialog();
            }
            private void selectImage(){
                final CharSequence[] items = {"Camera", "Gallery", "Cancel"};

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setTitle("Add Image");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(items[which].equals("Camera") ){
                            if(((MeActivity)getActivity()).checkPermissions(Permissions.CAMERA_PERMISSION[0])){
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, CAMERA_REQUEST_CODE);
                            }

                        }
                        else if (items[which].equals("Gallery") ){
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/*");
                            startActivityForResult(intent.createChooser(intent, "Select File"), STORAGE_REQUEST_CODE);
                        }
                        else if (items[which].equals("Cancel")){
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
            }

            private void showImageImportDialog(){

                String items[] = {" Camera ", " Gallery ", "Cancel"};
                android.support.v7.app.AlertDialog.Builder dialog = new android.support.v7.app.AlertDialog.Builder(context);

                dialog.setTitle("Select Image");
                dialog.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0){
                            if (!checkCameraPermission()){

                                requestCameraPermission();

                            }else{
                                pickCamera();
                            }
                        }
                        if (which == 1){
                            if(!checkStoragePermission()){
                                requestStoragePermission();
                            }else{
                                pickGallery();
                            }

                        }
                        if (which == 2) {
                            dialog.dismiss();
                        }
                    }
                });
                dialog.create().show();
            }

        });


    }

    private void pickGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_GALLERY_CODE);
    }

    private void pickCamera() {

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Pic ");
        values.put(MediaStore.Images.Media.DESCRIPTION, "Image to Text ");
        image_uri = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_CODE);
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(getActivity(), storagePermission, STORAGE_REQUEST_CODE);
    }

    private boolean checkStoragePermission() {
        boolean result2 = ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return  result2;
    }

    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(getActivity(), cameraPermission, CAMERA_REQUEST_CODE);
    }

    private boolean checkCameraPermission(){
        boolean result = ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.CAMERA) == (PackageManager.PERMISSION_GRANTED);
        boolean result2 = ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        return result && result2;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case CAMERA_REQUEST_CODE:
                if(grantResults.length > 0){
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if(cameraAccepted && writeStorageAccepted){
                        pickCamera();
                    }else{
                        Toast.makeText(getContext(), "permission denied ", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

            case STORAGE_REQUEST_CODE:
                if(grantResults.length > 0){
                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if( writeStorageAccepted){
                        pickGallery();
                    }else{
                        Toast.makeText(getContext(), "permission denied ", Toast.LENGTH_SHORT).show();
                    }
                }
                break;

        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode== Activity.RESULT_OK){
            if(resultCode==IMAGE_PICK_CAMERA_CODE){

                Bundle bundle = data.getExtras();
                final Bitmap bitmap = (Bitmap) bundle.get("data");
                iv_Image.setImageBitmap(bitmap);
                iv_Image.setVisibility(View.VISIBLE);
            }
            else if(requestCode==IMAGE_PICK_GALLERY_CODE){
                Uri selectImageUrl = data.getData();
                iv_Image.setImageURI(selectImageUrl);
                iv_Image.setVisibility(View.VISIBLE);
            }
        }
//        if(resultCode == RESULT_OK){
//            if(requestCode == IMAGE_PICK_GALLERY_CODE){
//                CropImage.activity(data.getData())
//                        .setGuidelines(CropImageView.Guidelines.ON)
//                        .start(getActivity(), this);
//            }
//            if(requestCode == IMAGE_PICK_CAMERA_CODE){
//                CropImage.activity(image_uri)
//                        .setGuidelines(CropImageView.Guidelines.ON)
//                        .start(getActivity(),this);
//            }
//        }
//        if(requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if(resultCode == RESULT_OK){
//                Uri resultUri = result.getUri();
//
//                iv_Image.setImageURI(resultUri);
//
//                BitmapDrawable bitmapDrawable = (BitmapDrawable)iv_Image.getDrawable();
//                Bitmap bitmap = bitmapDrawable.getBitmap();
//                iv_Image.setImageBitmap(bitmap);
//                iv_Image.setVisibility(View.VISIBLE);
////
////                TextRecognizer textRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
////
////                if(!textRecognizer.isOperational()){
////                    Toast.makeText(getApplicationContext(),"Error man ka loy!  ", Toast.LENGTH_SHORT).show();
////                }else{
////                    Frame frame = new Frame.Builder().setBitmap(bitmap).build();
////                    SparseArray<TextBlock> items = textRecognizer.detect(frame);
////
////                    StringBuilder sb = new StringBuilder();
////
////                    for(int i = 0; i<items.size(); i++){
////                        TextBlock myItem = items.valueAt(i);
////                        sb.append(myItem.getValue());
////                        sb.append("\n");
////                    }
////                    txtResult.setText(sb.toString());
//
//            }
//            else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
//                Exception error = result.getError();
//
//                Toast.makeText(getActivity(), "Error man LOY! " + error, Toast.LENGTH_SHORT).show();
//            }
//        }
    }
}
