package com.example.yesmobileapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yesmobileapp.Adapters.ViewCommentsAdapter;
import com.example.yesmobileapp.Models.Comments;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class ViewCommentsFragment extends Fragment {
    private View view;
    private RecyclerView recyclerView;
    private ArrayList<Comments> commentsList;
    private ViewCommentsAdapter viewCommentsAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedIntanceState){
        view = inflater.inflate(R.layout.fragment_view_comments,container,false);

        initializeUI();
        return view;
    }
    private void initializeUI() {
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        commentsList = new ArrayList<>();
        commentsList.add(new Comments(R.drawable.vito1, "Vito Bratta ", "Comments namba 1"));
        commentsList.add(new Comments(R.drawable.james1, "James Hitfield ","Comments namba 2"));
        commentsList.add(new Comments(R.drawable.billy2, "Billy Sheehan ","Comments namba 3"));
        commentsList.add(new Comments(R.drawable.joe1, "Joe Satriani ","Comments namba 4"));
        commentsList.add(new Comments(R.drawable.kirk1, "Kirk Hammett ","Comments namba 5"));


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        viewCommentsAdapter = new ViewCommentsAdapter(getContext(), commentsList);

        recyclerView.setAdapter(viewCommentsAdapter);
    }
}
