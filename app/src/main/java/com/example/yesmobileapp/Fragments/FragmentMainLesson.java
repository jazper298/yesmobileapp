package com.example.yesmobileapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yesmobileapp.Adapters.recycleViewAdapter;
import com.example.yesmobileapp.Models.recycleView;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class FragmentMainLesson extends Fragment {
    private View view;
    private Context context;
    private RecyclerView recyclerView;
    private ArrayList<recycleView> recyclerViewList;
    private com.example.yesmobileapp.Adapters.recycleViewAdapter recycleViewAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedIntanceState){

        view = inflater.inflate(R.layout.layout_main_lesson,container,false);
        context = getContext();
        initializeUI();
        return view;
    }
    private void initializeUI() {
        recyclerView = (RecyclerView)view.findViewById(R.id.recycler_view2);

        recyclerViewList = new ArrayList<>();
        recyclerViewList.add(new recycleView(R.drawable.goku1,R.drawable.goku1,R.drawable.goku1) );

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        recycleViewAdapter = new recycleViewAdapter(getContext(), recyclerViewList);

        recyclerView.setAdapter(recycleViewAdapter);
    }
}
