package com.example.yesmobileapp.Fragments;

import android.annotation.TargetApi;
import android.app.LoaderManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yesmobileapp.Activities.LoginActivity;
import com.example.yesmobileapp.Activities.ResetPasswordActivity;
import com.example.yesmobileapp.MainActivity;
import com.example.yesmobileapp.R;
import com.example.yesmobileapp.Utils.Debugger;
import com.example.yesmobileapp.Utils.HttpProvider;
import com.example.yesmobileapp.Utils.UserSession;
import com.example.yesmobileapp.Utils.Utility;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import es.dmoral.toasty.Toasty;

public class LoginStudentFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>  {
    //API LOGIN
    private View view;
    private Context context;
    private Button btnSignin;
    private ProgressDialog progressDialog;
    //private com.example.picpamobileapp.Models.UserProfile userProfile;

    private static final int REQUEST_READ_CONTACTS = 0;
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };

    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView etUsername;
    private EditText etPassword;
    //private Button btnRegister;
    private View mProgressView;
    private View mLoginFormView;

    public LoginStudentFragment() {

    }

    //FIREBASE LOGIN
//    private View view;
//    private Context context;
//    private Button btnSignIn;
//    private AutoCompleteTextView etUsername;
//    private EditText etPassword;
//
//    FirebaseAuth auth;
//    TextView forgot_password;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login_student, container,false);
        context = getContext();
        //initializeUI();
        return view;
    }
//    private void initializeUI(){
////        Toolbar toolbar = view.findViewById(R.id.toolbar);
////        setSupportActionBar(toolbar);
////        getSupportActionBar().setTitle("Login");
////        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//        auth = FirebaseAuth.getInstance();
//
//        etUsername = view.findViewById(R.id.etUsername);
//        etPassword = view.findViewById(R.id.etPassword);
//        btnSignIn = view.findViewById(R.id.btnSignIn);
//        forgot_password = view.findViewById(R.id.forgot_password);
//
//        forgot_password.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                startActivity(new Intent(getContext(), ResetPasswordActivity.class));
//            }
//        });
//
//        btnSignIn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String txt_email = etUsername.getText().toString();
//                String txt_password = etPassword.getText().toString();
//
//                if (TextUtils.isEmpty(txt_email) || TextUtils.isEmpty(txt_password)){
//                    Toast.makeText(getContext(), "All fileds are required", Toast.LENGTH_SHORT).show();
//                } else {
//
//                    auth.signInWithEmailAndPassword(txt_email, txt_password)
//                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
//                                @Override
//                                public void onComplete(@NonNull Task<AuthResult> task) {
//                                    if (task.isSuccessful()){
//                                        Intent intent = new Intent(getContext(), MainActivity.class);
//                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        startActivity(intent);
//                                        //finish();
//                                    } else {
//                                        Toast.makeText(getContext(), "Authentication failed!", Toast.LENGTH_SHORT).show();
//                                    }
//                                }
//                            });
//                }
//            }
//        });
//    }

    @Override
    public void onStart() {
        super.onStart();
        etUsername = view.findViewById(R.id.etUsername);
        etPassword = view.findViewById(R.id.etPassword);
        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });
        progressDialog = new ProgressDialog(getActivity());
        btnSignin = view.findViewById(R.id.btnSignIn);
        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //attemptLogin();
                //doLogin();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }
    public static LoginStudentFragment newInstance() {
        LoginStudentFragment fragment = new LoginStudentFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }
    private void attemptLogin()
    {
        try
        {
            if (mAuthTask != null) {
                return;
            }

            // Reset errors.
            etUsername.setError(null);
            etPassword.setError(null);

            // Store values at the time of the login attempt.
            String email = etUsername.getText().toString();
            String password = etPassword.getText().toString();

            boolean cancel = false;
            View focusView = null;

//            // Check for a valid password, if the user entered one.
//            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
//                etPassword.setError(getString(R.string.error_invalid_password));
//                focusView = etPassword;
//                cancel = true;
//            }

            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                etUsername.setError(getString(R.string.error_field_required));
                focusView = etUsername;
                cancel = true;
            } else if (!isEmailValid(email)) {
                etUsername.setError(getString(R.string.error_invalid_email));
                focusView = etUsername;
                cancel = true;
            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.

                JSONObject jsonParams = new JSONObject();

                jsonParams.put("username", email);
                jsonParams.put("password", password);

                StringEntity entity = new StringEntity(jsonParams.toString());

                if(!Utility.haveNetworkConnection(context)){
                    etUsername.setError("Internet connection is required");
                    etUsername.requestFocus();
                }else {
                    Debugger.printO(jsonParams.toString());
                    doLogin(entity);
                    hideKeyBoard();
                    showProgress(true);
                }
            }

        } catch (Exception err)
        {
            Toasty.error(getContext(), err.toString()).show();
        }
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
//            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
//
//            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
//                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//                }
//            });
//
//            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
//            mProgressView.animate().setDuration(shortAnimTime).alpha(
//                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
//                @Override
//                public void onAnimationEnd(Animator animation) {
//                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
//                }
//            });
//        } else {
//            // The ViewPropertyAnimator APIs are not available, so simply show
//            // and hide the relevant UI components.
//            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
//            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
//        }
    }
    private void doLogin(StringEntity stringEntity){
        progressDialog.show();
        progressDialog.setMessage("Logging In");
        HttpProvider.post(getContext(), "post/userlogin", stringEntity,"application/json", new JsonHttpResponseHandler(){

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    boolean isSuccess = response.getBoolean("success");
                    if(isSuccess) {
                        progressDialog.hide();
                        Toasty.success(context, "You are now logged in ", Toast.LENGTH_SHORT).show();
                        UserSession session = new Gson().fromJson(response.toString(), new TypeToken<UserSession>() {
                        }.getType());

                        Debugger.logD("wtf " + response.toString());

                        session.saveUserSession(context);
                        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putInt("FIRST_LOGIN", 1);
                        editor.commit();


                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();

                        showProgress(false);
                    } else{
                        progressDialog.hide();
                        showProgress(false);
                        etPassword.setError(getString(R.string.error_field_credentials));
                        etPassword.requestFocus();
                    }

                }catch (Exception err){
                    Debugger.logD("login Error: "+err);
                }

            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                super.onSuccess(statusCode, headers, response);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                progressDialog.hide();
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toasty.error(context, throwable.toString()).show();
                Debugger.logD("shit" + throwable.toString());
                showProgress(false);
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                progressDialog.hide();
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Toasty.error(context, errorResponse.toString()).show();
                Debugger.logD("shit" + errorResponse.toString());
                showProgress(false);
            }


            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progressDialog.hide();
                super.onFailure(statusCode, headers, responseString, throwable);
                Toasty.error(context, throwable.toString()).show();
                Debugger.logD("shit" + responseString);
                showProgress(false);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                progressDialog.hide();
                super.onSuccess(statusCode, headers, responseString);
            }
        });
    }
    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        //return email.contains("@");
        return true;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(getContext(),
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        List<String> emails = new ArrayList<>();
        data.moveToFirst();
        while (!data.isAfterLast()) {
            emails.add(data.getString(ProfileQuery.ADDRESS));
            data.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(context,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        etUsername.setAdapter(adapter);
    }


    private class UserLoginTask {
    }
    //Hide Keyboard when submit credentials
    private void hideKeyBoard(){
        try
        {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert inputManager != null;
            //inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }
}
