package com.example.yesmobileapp.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yesmobileapp.Adapters.AlltimeRankingAdapter;
import com.example.yesmobileapp.Adapters.LessonAdapter;
import com.example.yesmobileapp.Adapters.TeachLessonAdapter;
import com.example.yesmobileapp.Models.Alltime;
import com.example.yesmobileapp.Models.Lesson;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class LessonFragment  extends Fragment {
    private static final String TAG = "LessonFragment";
    private View view;
    private Context context;
    private RecyclerView recyclerView;
    private ArrayList<Lesson> lessonsList;
    private TeachLessonAdapter lessonAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedIntanceState){
         view = inflater.inflate(R.layout.fragment_lesson,container,false);
        initializeUI();
        return view;
    }
    private void initializeUI(){
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        lessonsList = new ArrayList<>();
        lessonsList.add(new Lesson(R.drawable.goku1, R.drawable.ic_ellipses,R.drawable.soo_in,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"Soo-in-Lee ", "Liked by Goku, Vegeta, Metallica and 30 others ", "#kpop #soo-in #loveleeyy", "View all 3 comments", "3 DAYS AGO"));
        lessonsList.add(new Lesson(R.drawable.john1, R.drawable.ic_ellipses,R.drawable.goku1,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"John Petrucci ", "700 Likes", "#Dream Theater", "View All 6 Comments", "3 DAYS AGO"));
        lessonsList.add(new Lesson(R.drawable.soo_in, R.drawable.ic_ellipses,R.drawable.tony,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"Soo-in-Lee ", "1 M You and 1 million Others ", "#Tony #Rockstar #Gwapo", "View All 1M Comments", "1 DAY AGO"));
        lessonsList.add(new Lesson(R.drawable.vito1, R.drawable.ic_ellipses,R.drawable.ard,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"Vito Bratta ", "1 B Likes ", "#Gradwaiting #Sila", "View All 1B Comments", "1 HOUR AGO"));
        lessonsList.add(new Lesson(R.drawable.james1, R.drawable.ic_ellipses,R.drawable.kirk1,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"James Hitfield ", "Liked by You and 200 Others", "#Metallica", "View All 100 Comments", "6 DAYS AGO"));
        lessonsList.add(new Lesson(R.drawable.save1, R.drawable.ic_ellipses,R.drawable.marty1,R.drawable.ic_heart_red,R.drawable.ic_heart_white,R.drawable.ic_speech_bubble,"Dave Mustaine ", "Liked by You and 180 Others ", "#Megadeth #Metal", "View All 500 Comments", "2 DAYS AGO"));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        lessonAdapter = new TeachLessonAdapter(getContext(), lessonsList);

        recyclerView.setAdapter(lessonAdapter);
    }
}
