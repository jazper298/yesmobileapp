package com.example.yesmobileapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.yesmobileapp.Adapters.TodayRankingAdapter;
import com.example.yesmobileapp.Models.Today;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class TodayFragment extends Fragment {
    private static final String TAG = "Tab1Fragment";

    private Button btnClick;
    private View view;
    private RecyclerView recyclerView;
    private ArrayList<Today> todayList;
    private TodayRankingAdapter todayRankingAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedIntanceState){
        view = inflater.inflate(R.layout.fragment_today,container,false);

        initializeUI();
        return view;
    }
    private void initializeUI(){
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        todayList = new ArrayList<>();

        todayList.add(new Today(R.drawable.john1, "John Petrucci ", "700 pts "));
        todayList.add(new Today(R.drawable.james1, "James Hitfield ", "650 pts "));
        todayList.add(new Today(R.drawable.joe1, "Joe Satriani ", "600 pts "));
        todayList.add(new Today(R.drawable.kirk1, "Kirk Hammett ", "550 pts "));
        todayList.add(new Today(R.drawable.marty1, "Marty Friedman ", "530 pts "));
        todayList.add(new Today(R.drawable.paul1, "Paul Gilbert ", "500 pts "));
        todayList.add(new Today(R.drawable.save1, "Dave Mustaine ", "480 pts "));
        todayList.add(new Today(R.drawable.steve1, "Steve Vai ", "450 pts "));
        todayList.add(new Today(R.drawable.vito1, "Vito Bratta ", "430 pts "));
        todayList.add(new Today(R.drawable.yngwie1, "Yngwie Malmsten ", "300 pts "));
        todayList.add(new Today(R.drawable.joe2, "Joe Satriani ", "350 pts "));
        todayList.add(new Today(R.drawable.billy2, "Billy Sheehan ", "250 pts "));


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        todayRankingAdapter = new TodayRankingAdapter(getContext(), todayList);

        recyclerView.setAdapter(todayRankingAdapter);
    }
}
