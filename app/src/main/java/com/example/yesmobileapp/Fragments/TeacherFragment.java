package com.example.yesmobileapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.yesmobileapp.R;

public class TeacherFragment  extends Fragment {
    private static final String TAG = "Tab1Fragment";
    private Button btnClick;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedIntanceState){
        View view = inflater.inflate(R.layout.fragment_teacher,container,false);
        btnClick = (Button)view.findViewById(R.id.btnClick1);
        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),"TESTING BUTTON CLICK 1", Toast.LENGTH_SHORT).show();
            }
        });
        return view;
    }
}
