package com.example.yesmobileapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.yesmobileapp.Adapters.AlltimeRankingAdapter;
import com.example.yesmobileapp.Models.Alltime;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class AllTimeFragment extends Fragment {
    private static final String TAG = "Tab1Fragment";
    private Button btnClick;

    private View view;
    private RecyclerView recyclerView;
    private ArrayList<Alltime> alltimesList;
    private AlltimeRankingAdapter alltimeRankingAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedIntanceState){
        view = inflater.inflate(R.layout.fragment_alltime,container,false);

        initializeUI();
        return view;
    }
    private void initializeUI(){
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        alltimesList = new ArrayList<>();

        alltimesList.add(new Alltime(R.drawable.john1, "John Petrucci ", "700 pts "));
        alltimesList.add(new Alltime(R.drawable.joe2, "Joe Satriani ", "350 pts "));
        alltimesList.add(new Alltime(R.drawable.joe1, "Joe Satriani ", "600 pts "));

        alltimesList.add(new Alltime(R.drawable.save1, "Dave Mustaine ", "480 pts "));
        alltimesList.add(new Alltime(R.drawable.yngwie1, "Yngwie Malmsten ", "300 pts "));

        alltimesList.add(new Alltime(R.drawable.marty1, "Marty Friedman ", "530 pts "));
        alltimesList.add(new Alltime(R.drawable.paul1, "Paul Gilbert ", "500 pts "));
        alltimesList.add(new Alltime(R.drawable.steve1, "Steve Vai ", "450 pts "));
        alltimesList.add(new Alltime(R.drawable.vito1, "Vito Bratta ", "430 pts "));
        alltimesList.add(new Alltime(R.drawable.james1, "James Hitfield ", "650 pts "));
        alltimesList.add(new Alltime(R.drawable.billy2, "Billy Sheehan ", "250 pts "));
        alltimesList.add(new Alltime(R.drawable.joe1, "Joe Satriani ", "600 pts "));
        alltimesList.add(new Alltime(R.drawable.kirk1, "Kirk Hammett ", "550 pts "));


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        alltimeRankingAdapter = new AlltimeRankingAdapter(getContext(), alltimesList);

        recyclerView.setAdapter(alltimeRankingAdapter);
    }
}
