package com.example.yesmobileapp.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.yesmobileapp.Adapters.LessonAdapter;
import com.example.yesmobileapp.Adapters.VideoAdapter;
import com.example.yesmobileapp.Models.Lesson;
import com.example.yesmobileapp.Models.Videos;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class VideosFragment extends Fragment {
    private static final String TAG = "VideosFragment";
    private View view;
    private Context context;
    private RecyclerView recyclerView;
    private ArrayList<Videos> videosList;
    private VideoAdapter videoAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedIntanceState){
        view = inflater.inflate(R.layout.fragment_videos,container,false);
        initializeUI();
        return view;
    }
    private void initializeUI(){
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        videosList = new ArrayList<>();
        videosList.add(new Videos(R.drawable.soo_in, R.drawable.ic_ellipses,R.raw.solo,"Son Goku ", "Love Story in Harvard ", "View all 3 comments"));
//        videosList.add(new Videos(R.drawable.soo_in, R.drawable.ic_ellipses,R.raw.kpop,"Soo-in-Lee ", "The Making of a Rockstar ", "View all 1000 comments"));
//        videosList.add(new Videos(R.drawable.vegeta1, R.drawable.ic_ellipses,R.drawable.ard,"Vegeta ", "Life in the fast Lane ", "View all 50 comments"));
//        videosList.add(new Videos(R.drawable.john1, R.drawable.ic_ellipses,R.drawable.john1,"John Petrucci", "Metropolis, The Miracle and the Sleeper", "View all 130 comments"));
//        videosList.add(new Videos(R.drawable.kirk1, R.drawable.ic_ellipses,R.drawable.james1,"Kirk Hammett ", "And Justice for All ", "View all 10 comments"));
//        videosList.add(new Videos(R.drawable.paul1, R.drawable.ic_ellipses,R.drawable.billy2,"Paul Gilbert ", "Addicted to that Rush ", "View all 43 comments"));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        videoAdapter = new VideoAdapter(getContext(), videosList);

        recyclerView.setAdapter(videoAdapter);
    }
}
