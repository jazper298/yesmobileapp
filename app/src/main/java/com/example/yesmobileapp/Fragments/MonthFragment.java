package com.example.yesmobileapp.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.yesmobileapp.Adapters.MonthRankingAdapter;
import com.example.yesmobileapp.Models.Month;
import com.example.yesmobileapp.R;

import java.util.ArrayList;

public class MonthFragment extends Fragment {
    private static final String TAG = "Tab1Fragment";
    private Button btnClick;

    private View view;
    private RecyclerView recyclerView;
    private ArrayList<Month> monthsList;
    private MonthRankingAdapter monthRankingAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedIntanceState){
        view = inflater.inflate(R.layout.fragment_month,container,false);

        initializeUI();
        return view;
    }
    private void initializeUI(){
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        monthsList = new ArrayList<>();

        monthsList.add(new Month(R.drawable.john1, "John Petrucci ", "700 pts "));
        monthsList.add(new Month(R.drawable.steve1, "Steve Vai ", "450 pts "));
        monthsList.add(new Month(R.drawable.vito1, "Vito Bratta ", "430 pts "));
        monthsList.add(new Month(R.drawable.james1, "James Hitfield ", "650 pts "));
        monthsList.add(new Month(R.drawable.joe1, "Joe Satriani ", "600 pts "));
        monthsList.add(new Month(R.drawable.kirk1, "Kirk Hammett ", "550 pts "));
        monthsList.add(new Month(R.drawable.save1, "Dave Mustaine ", "480 pts "));
        monthsList.add(new Month(R.drawable.yngwie1, "Yngwie Malmsten ", "300 pts "));
        monthsList.add(new Month(R.drawable.joe2, "Joe Satriani ", "350 pts "));
        monthsList.add(new Month(R.drawable.joe1, "Joe Satriani ", "600 pts "));
        monthsList.add(new Month(R.drawable.billy2, "Billy Sheehan ", "250 pts "));
        monthsList.add(new Month(R.drawable.marty1, "Marty Friedman ", "530 pts "));
        monthsList.add(new Month(R.drawable.paul1, "Paul Gilbert ", "500 pts "));


        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        RecyclerView.LayoutManager rvLayoutManager = layoutManager;
        recyclerView.setLayoutManager(rvLayoutManager);

        monthRankingAdapter = new MonthRankingAdapter(getContext(), monthsList);

        recyclerView.setAdapter(monthRankingAdapter);
    }
}
