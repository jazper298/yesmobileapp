package com.example.yesmobileapp.Helpers;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.util.Log;
import android.view.MenuItem;

import com.example.yesmobileapp.Activities.ClassmatesActivity;
import com.example.yesmobileapp.Activities.FindActivity;
import com.example.yesmobileapp.Activities.MeActivity;
import com.example.yesmobileapp.Activities.NotesActivity;
import com.example.yesmobileapp.Activities.RankingsActivity;
import com.example.yesmobileapp.MainActivity;
import com.example.yesmobileapp.R;
import com.ittianyu.bottomnavigationviewex.BottomNavigationViewEx;

public class BottomNavigationViewHelper {
    private static final String TAG = "BottomNavigationViewHel";

    public static void setupBottomNavigationView(BottomNavigationViewEx bottomNavigationViewEx){
        Log.d(TAG, "setupBottomNavigationView: Setting up BottomNavigationView");
        bottomNavigationViewEx.enableAnimation(false);
        bottomNavigationViewEx.enableItemShiftingMode(false);
        bottomNavigationViewEx.enableShiftingMode(false);
        bottomNavigationViewEx.setTextVisibility(false);
    }
    public static void enableNavigation(final Context context, BottomNavigationViewEx view){
        view.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){

                    case R.id.ic_house:
                        Intent intent1 = new Intent(context, MainActivity.class);//ACTIVITY_NUM = 0
                        context.startActivity(intent1);

                        break;
//                    case R.id.ic_notes:
//                        Intent intent5 = new Intent(context, NotesActivity.class);//ACTIVITY_NUM = 1
//                        context.startActivity(intent5);
//                        break;

                    case R.id.ic_classmates:
                        Intent intent2  = new Intent(context, ClassmatesActivity.class);//ACTIVITY_NUM = 2
                        context.startActivity(intent2);
                        break;

                    case R.id.ic_rank:
                        Intent intent3 = new Intent(context, RankingsActivity.class);//ACTIVITY_NUM = 3
                        context.startActivity(intent3);
                        break;

                    case R.id.ic_me:
                        Intent intent4 = new Intent(context, MeActivity.class);//ACTIVITY_NUM = 4
                        context.startActivity(intent4);
                        break;

                }

                return false;
            }
        });
    }
}
