package com.example.yesmobileapp.Models;

public class Videos {
    private int ivProfilePicture, ivEllipses, ivMainVideo;
    private int ivMainVideos;
    private  String txtProfileName, txtDescription, txtViewComment;

    public Videos(int ivProfilePicture,int ivEllipses,int ivMainVideos,
                  String txtProfileName, String txtDescription, String txtViewComment ){
        this.ivProfilePicture = ivProfilePicture;
        this.ivEllipses = ivEllipses;
        this.ivMainVideos = ivMainVideos;

        this.txtProfileName = txtProfileName;
        this.txtDescription = txtDescription;
        this.txtViewComment = txtViewComment;


    }
    public int getProfilePicture() {
        return ivProfilePicture;
    }
    public int getEllipses() {
        return ivEllipses;
    }
    public int getMainVideo() {
        return ivMainVideos;
    }

    public String getProfileName() {
        return txtProfileName;
    }
    public String getDescription() {
        return txtDescription;
    }
    public String getViewComment() {
        return txtViewComment;
    }

    public void setProfilePicture(int ivProfilePicture) {
        this.ivProfilePicture = ivProfilePicture;
    }
    public void setEllipses(int ivEllipses) {
        this.ivEllipses = ivEllipses;
    }
    public void setMainVideo(int ivMainVideo) {
        this.ivMainVideo = ivMainVideo;
    }

    public void setProfileName(String txtProfileName) {
        this.txtProfileName = txtProfileName;
    }
    public void setDescription(String txtDescription) {
        this.txtDescription = txtDescription;
    }
    public void setViewComment(String txtViewComment) {
        this.txtViewComment = txtViewComment;
    }
}
