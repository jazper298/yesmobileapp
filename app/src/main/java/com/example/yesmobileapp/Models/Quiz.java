package com.example.yesmobileapp.Models;

public class Quiz {
    private int ivProfilePicture, ivEllipses;
    private  String txtProfileName, txtQuizTitle, txtClock, txtItems;

    public Quiz(int ivProfilePicture,int ivEllipses,
                  String txtProfileName, String txtQuizTitle, String txtClock, String txtItems ){
        this.ivProfilePicture = ivProfilePicture;
        this.ivEllipses = ivEllipses;

        this.txtProfileName = txtProfileName;
        this.txtQuizTitle = txtQuizTitle;
        this.txtClock = txtClock;
        this.txtItems = txtItems;

    }
    public int getProfilePicture() {
        return ivProfilePicture;
    }
    public int getEllipses() {
        return ivEllipses;
    }

    public String getProfileName() {
        return txtProfileName;
    }
    public String getQuizTitle() {
        return txtQuizTitle;
    }
    public String getClock() {
        return txtClock;
    }
    public String getItems() {
        return txtItems;
    }

    public void setProfilePicture(int ivProfilePicture) {
        this.ivProfilePicture = ivProfilePicture;
    }
    public void setEllipses(int ivEllipses) {
        this.ivEllipses = ivEllipses;
    }

    public void setProfileName(String txtProfileName) {
        this.txtProfileName = txtProfileName;
    }
    public void setQuizTitle(String txtQuizTitle) {
        this.txtQuizTitle = txtQuizTitle;
    }
    public void setClock(String txtClock) {
        this.txtClock = txtClock;
    }
    public void setItems(String txtItems) {
        this.txtItems = txtItems;
    }
}
