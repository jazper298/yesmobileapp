package com.example.yesmobileapp.Models;

public class Lesson  {
    private int ivProfilePicture, ivEllipses, ivMainImage,  ivHeartRed, ivHeartWhite, ivSpeechBubble;
    private  String txtProfileName, txtLikes, txtHastag, txtViewComment, txtTimePosted;

    public Lesson(int ivProfilePicture,int ivEllipses,int ivMainImage,int ivHeartRed,int ivHeartWhite,int ivSpeechBubble,
                  String txtProfileName, String txtLikes, String txtHastag, String txtViewComment,String txtTimePosted){
        this.ivProfilePicture = ivProfilePicture;
        this.ivEllipses = ivEllipses;
        this.ivMainImage = ivMainImage;
        this.ivHeartRed = ivHeartRed;
        this.ivHeartWhite = ivHeartWhite;
        this.ivSpeechBubble = ivSpeechBubble;
        this.txtProfileName = txtProfileName;
        this.txtLikes = txtLikes;
        this.txtHastag = txtHastag;
        this.txtViewComment = txtViewComment;
        this.txtTimePosted = txtTimePosted;

    }
    public int getProfilePicture() {
        return ivProfilePicture;
    }
    public int getEllipses() {
        return ivEllipses;
    }
    public int getMainImage() {
        return ivMainImage;
    }
    public int getHeartRed() {
        return ivHeartRed;
    }
    public int getHeartWhite() {
        return ivHeartWhite;
    }
    public int getSpeechBubble() {
        return ivSpeechBubble;
    }

    public String getProfileName() {
        return txtProfileName;
    }
    public String getLikes() {
        return txtLikes;
    }
    public String getHastag() {
        return txtHastag;
    }
    public String getViewComment() {
        return txtViewComment;
    }
    public String getTimePosted() {
        return txtTimePosted;
    }

    public void setProfilePicture(int ivProfilePicture) {
        this.ivProfilePicture = ivProfilePicture;
    }
    public void setEllipses(int ivEllipses) {
        this.ivEllipses = ivEllipses;
    }
    public void setMainImage(int ivMainImage) {
        this.ivMainImage = ivMainImage;
    }
    public void setHeartRed(int ivHeartRed) {
        this.ivHeartRed = ivHeartRed;
    }
    public void setHeartWhite(int ivHeartWhite) {
        this.ivHeartWhite = ivHeartWhite;
    }
    public void setSpeechBubble(int ivSpeechBubble) {
        this.ivSpeechBubble = ivSpeechBubble;
    }
    public void setProfileName(String txtProfileName) {
        this.txtProfileName = txtProfileName;
    }
    public void setLikes(String txtLikes) {
        this.txtLikes = txtLikes;
    }
    public void setHastag(String txtHastag) {
        this.txtHastag = txtHastag;
    }
    public void setViewComment(String txtViewComment) {
        this.txtViewComment = txtViewComment;
    }
    public void setTimePosted(String txtTimePosted) {
        this.txtTimePosted = txtTimePosted;
    }
}
